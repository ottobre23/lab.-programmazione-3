import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


public class InnerClass {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		shell.setSize(100, 500);
		shell.pack();
		shell.open();
		
		Button b1 = new Button(shell, SWT.PUSH);
		b1.setText("OK");
		b1.setBounds(0, 0, 100, 30);
		
//		b1.addSelectionListener(new PrintSelectionAdapter());
		
//		b1.addSelectionListener(new ExitSelectionAdapter());
		
		b1.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					System.out.println(e);
				}
			}
		);
		

		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
	}
	
	
}

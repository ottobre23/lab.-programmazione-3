
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.graphics.Color;

public class Labels {
	public static void wellPopulate(Shell s) {
		Label l1 = new Label(s, SWT.BORDER);
		l1.setText("uno");
		l1.setBounds(0, 0, 100, 30);
		Label l2 = new Label(s, SWT.SEPARATOR | SWT.SHADOW_IN);
		l2.setText("uno e mezzo");
		l2.setBounds(0, 30, 100, 3);
		Label l3 = new Label(s, SWT.SEPARATOR | SWT.SHADOW_OUT);
		l3.setBounds(0, 45, 100, 30);
		Label l4 = new Label(s, SWT.NONE | SWT.SHADOW_ETCHED_IN);
		l4.setText("due");
		l4.setBounds(20, 160, 60,30);
		//l4.setBounds(20, 65, 60,30);
		l4.setBackground(new Color(s.getDisplay(), 200, 200, 0));
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		shell.setSize(100, 300);
		shell.open();
		wellPopulate(shell);
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch())
				display.sleep();
		}
		
		display.dispose();
	}


}


import org.eclipse.swt.widgets.Display;

import it.unimi.mat.mediator.UpDownShell;
import it.unimi.mat.command.PrintCommand;

public class MediatorTester {

	public static void main(String[] args) {		
		
		Display display = new Display();
		PrintCommand p = new PrintCommand();
		UpDownShell shell = new UpDownShell(display,p);
		
		shell.open();
		shell.pack();
		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();

	}
	
}

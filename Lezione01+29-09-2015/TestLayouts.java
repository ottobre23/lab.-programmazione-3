
import org.eclipse.swt.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class TestLayouts {
	
	public static void gridPopulate(Shell s) {
		GridLayout g = new GridLayout();
		g.numColumns = 2;
		s.setLayout(g);
		
		Label l1 = new Label(s, SWT.NONE);
		l1.setText("Nome ");
		Text t1 = new Text(s, SWT.BORDER);
		
		Label l2 = new Label(s, SWT.NONE);
		l2.setText("Et� ");
		Text t2 = new Text(s, SWT.BORDER);
		
		Label l3 = new Label(s, SWT.NONE);
		l3.setText("Sesso ");
		Text t3 = new Text(s, SWT.BORDER);
		
		GridData dL = new GridData();
		dL.widthHint = 60;
		l1.setLayoutData(dL);
		l2.setLayoutData(dL);
		l3.setLayoutData(dL);
		
		GridData dT = new GridData(GridData.FILL_HORIZONTAL);
		t1.setLayoutData(dT);
		t2.setLayoutData(dT);
		t3.setLayoutData(dT);
		
		Button b = new Button(s, SWT.CHECK);
		b.setText("Impiegato negli ultimi sei mesi.");
		
		GridData dB = new GridData();
		dB.horizontalSpan = 2;
		b.setLayoutData(dB);
	}
	
	public static void populate(Shell s) {
		s.setLayout(new FillLayout(SWT.VERTICAL));
//		s.setLayout(new FillLayout(SWT.HORIZONTAL));
//		RowLayout r = new RowLayout(SWT.VERTICAL);
//		r.wrap = false;
//		s.setLayout(r);
		
		Label l0 = new Label(s, SWT.NONE);
		l0.setText("zero");
		Label l1 = new Label(s, SWT.NONE);
		l1.setText("uno");
		Label l2 = new Label(s, SWT.NONE);
		l2.setText("duedueduedue");
		Label l3 = new Label(s, SWT.NONE);
		l3.setText("tre");
		Label l4 = new Label(s, SWT.NONE);
		l4.setText("quattro");
		Label l5 = new Label(s, SWT.NONE);
		l5.setText("cinque cinque cinque cinque cinque");
	}
	
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		shell.setSize(100, 500);
		populate(shell);
//		gridPopulate(shell);
		shell.pack();
		shell.open();
		
		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
	}
}


import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;


public class TestSWT {

	public static void main(String[] args) throws InterruptedException {
	Display display = new Display();
	Shell shell = new Shell(display);
	
	shell.setSize(1000, 100);
	shell.open();
	
	 for(int s=1;s<500;s+=10) {
		shell.setSize(s, s);
		Thread.sleep(50);
	 }
	 

	
	 for(int ns=0; ns<5; ns++) {
		Shell s = new Shell(display);
		s.setBounds(60*ns, 100*(ns+2), 250, 250);
		Label l = new Label(s, SWT.NONE);
		l.setText("io sono la finestra numero " + (ns+1));
		l.pack();
		s.pack();
		s.open();
	 }
	
	while(!shell.isDisposed()) {
		if(!display.readAndDispatch()) 
		display.sleep();
	}
	
	display.dispose();
}	
	
}

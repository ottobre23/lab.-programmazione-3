package it.unimi.mat.command;

public class PrintCommand implements Command {
	public void execute(int n) {
		for(int i=0; i<n; i++)
			System.out.println("esecuzione numero " + (i+1));
	}
}

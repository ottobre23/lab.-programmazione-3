
import java.io.*;

public class IOTest {
	
	private static void scrivi() {
		try {
			FileOutputStream f = new FileOutputStream("data.out");
			DataOutputStream d = new DataOutputStream(f);
			
			d.writeDouble(2.54234);
			d.writeChar('\t');
			d.writeInt(42);
			
			d.close();
			f.close();
			
			
			f = new FileOutputStream("data.txt");
			PrintWriter p = new PrintWriter(f);
			
			p.print(2.54234);
			p.print('\t');
			p.print(42);			
			
			p.close();
			f.close();
			
		} catch(IOException e) {
			e.printStackTrace();
		} 
	}
	
	private static void leggi() {
		
		try {
			BufferedReader r = new BufferedReader(new FileReader("data.txt"));
			String line;
			while((line = r.readLine()) != null)
				System.out.println(line);
			
			DataInputStream i = new DataInputStream(new FileInputStream("data.out"));
			System.out.println(i.readDouble());
			System.out.println(i.readChar());
			System.out.println(i.readInt());
			
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		
	}
 	
	public static void main(String args[]) {
		
		scrivi();
		leggi();

	}
	
}

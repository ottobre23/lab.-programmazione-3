
import it.unimi.mat.command.LoadCommand;
import it.unimi.mat.command.SaveCommand;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;



public class MenuBarShell extends Shell {
		
		@Override
		protected void checkSubclass() {
			return;
		}
		
		Display display;
		
		private SelectionAdapter loadAdapter = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new LoadCommand(MenuBarShell.this).execute();
			}
		};
		
		private SelectionAdapter saveAdapter = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new SaveCommand(MenuBarShell.this).execute();
			}
		};
		
		public MenuBarShell(Display d) {
			super(d);
			display = d;
			
			Menu menu = new Menu(this, SWT.BAR);
			this.setMenuBar(menu);
			MenuItem file = new MenuItem(menu, SWT.CASCADE);
			file.setText("File");
			
			Menu fileMenu = new Menu(this, SWT.DROP_DOWN);
			file.setMenu(fileMenu);
			
			MenuItem load = new MenuItem(fileMenu, SWT.PUSH);
			load.setText("&Carica...\tCtrl+C");
			load.setAccelerator(SWT.CTRL+'C');
			load.addSelectionListener(loadAdapter);
			Image iLoad = new Image(display, "load.jpeg");
			Image iLoadScaled = new Image(display, iLoad.getImageData().scaledTo(30, 20));
			load.setImage(iLoadScaled);
			iLoad.dispose();
			
			MenuItem save = new MenuItem(fileMenu, SWT.PUSH);
			save.setText("&Salva...\tCtrl+S");
			save.setAccelerator(SWT.CTRL+'S');
			save.addSelectionListener(saveAdapter);
			Image iSave = new Image(display, "save.jpeg");
			Image iSaveScaled = new Image(display, iSave.getImageData().scaledTo(30, 20));
			save.setImage(iSaveScaled);
			iSave.dispose();
			
			MenuItem sep = new MenuItem(fileMenu, SWT.SEPARATOR);
			
			MenuItem exit = new MenuItem(fileMenu, SWT.PUSH);
			exit.setText("Esci\tCtrl+Q");
			exit.setAccelerator(SWT.CTRL+'Q');
			exit.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						MenuBarShell.this.close();
						MenuBarShell.this.dispose();
					}
				}
			);
			
			Menu popup = new Menu(this, SWT.POP_UP);
			MenuItem pLoad = new MenuItem(popup, SWT.PUSH);
			pLoad.setText("&Carica...\tCtrl+C");
			pLoad.setAccelerator(SWT.CTRL+'C');
			pLoad.addSelectionListener(loadAdapter);
			pLoad.setImage(iLoadScaled);
			MenuItem pSep = new MenuItem(popup, SWT.SEPARATOR);
			MenuItem pSave = new MenuItem(popup, SWT.PUSH);
			pSave.setText("Salva...\tCtrl+S");
			pSave.setAccelerator(SWT.CTRL+'S');
			pSave.addSelectionListener(saveAdapter);
			pSave.setImage(iSaveScaled);
			this.setMenu(popup);
			iLoadScaled.dispose();
			iSaveScaled.dispose();
			
			
			this.pack();
			this.open();
		}
		
		public static void main(String args[]) {
			Display display = Display.getDefault();
			MenuBarShell mbs = new MenuBarShell(display);
			
			while(!mbs.isDisposed()) {
				if(!display.readAndDispatch()) 
					display.sleep();
			}
			
			display.dispose();
		}

	}


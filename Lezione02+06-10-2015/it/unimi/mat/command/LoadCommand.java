package it.unimi.mat.command;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class LoadCommand extends MenuCommand {
	
	public LoadCommand(Shell s) {
		super(s);
	}
	
	public void execute() {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		dialog.setText("Apri file");
		String fileName = dialog.open();
			
		if( fileName == null ) {
			System.out.println("Apertura file annullata.");
			return;
		}
			
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(fileName);
			in = new ObjectInputStream(fis);
				
				
			
			in.close();
		} catch(IOException ex1) {
			System.out.println("Non ho potuto caricare l'oggetto (errore nel filesystem).");
		}
//		catch(ClassNotFoundException ex2) {
//			System.out.println("Il contenuto del file � corrotto.");
//		}
			
		System.out.println("L'oggetto � stato caricato.");
	}
}

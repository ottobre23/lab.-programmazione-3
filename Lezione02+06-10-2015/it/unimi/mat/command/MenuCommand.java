package it.unimi.mat.command;

import org.eclipse.swt.widgets.Shell;

public abstract class MenuCommand {

	protected Shell shell;
	
	public MenuCommand(Shell mbs) {
		shell = mbs;
	}
	public abstract void execute();
}

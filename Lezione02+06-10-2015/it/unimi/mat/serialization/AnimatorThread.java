package it.unimi.mat.serialization;


public class AnimatorThread {
	
	private int animatorFrames;
	private int currentFrame;
	
	public AnimatorThread(int animatorFrames) {
		this.animatorFrames = animatorFrames;
		this.currentFrame = 0;
	}
	
	public int getFrame() {
		return currentFrame;
	}
	
	public void setFrame(int f) {
		currentFrame = f;
	}
	
	public void run() {
		int numFrames = (int)(Math.random()*25);
		while(numFrames-- > 0) {
			currentFrame = ++currentFrame % 101;
			System.out.println(currentFrame);
		}
		
	}
}


import it.unimi.mat.designpatterns.factorymethod.LoggerFactory;
import it.unimi.mat.designpatterns.factorymethod.Logger;

public class LoggerTester {
	public static void main(String args[]) {
		LoggerFactory factory = new LoggerFactory();
		Logger logger = factory.getLogger();
		
		logger.log("A message to log");
	}
}

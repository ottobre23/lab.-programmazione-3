package it.unimi.mat;

public class HelloWorld {

	public static String message() {
		return "Hello, world!";
	}
	
	static String packageMessage() {
		return "Hello, package!";
	}
	
	public static void main(String args[]) {
		System.out.println("I'm the HelloWorld class!");
	}
	
}

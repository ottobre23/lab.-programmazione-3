package it.unimi.mat.designpatterns.abstractfactory;

public interface Car {
	public String getCarName();
	public String getCarFeatures();
}

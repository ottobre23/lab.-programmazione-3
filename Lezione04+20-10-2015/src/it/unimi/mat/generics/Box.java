package it.unimi.mat.generics;

public class Box<T> {

	private T content;
	
	public void add(T t) {
		content = t;
	}
	
	public T get() {
		return content;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked", "unused"})
	public static void main(String args[]) {
		Box<Integer> integerBox = new Box<Integer>();
		integerBox.add(4);
		
		int someInt = integerBox.get();
		System.out.println(someInt);
		
//		ora questo errore viene intercettato dal compilatore
//		String someString = (String)integerBox.get();
//		System.out.println(someString);
		
		//legacy
		Box<Integer> bi = new Box();
		Box bb = new Box<String>();
		Box bc = new Box();
	}
	
}

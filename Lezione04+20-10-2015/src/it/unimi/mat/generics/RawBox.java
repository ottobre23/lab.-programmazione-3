package it.unimi.mat.generics;

public class RawBox {

	private Object content;
	
	public void add(Object o) {
		content = o;
	}
	
	public Object get() {
		return content;
	}
	
	public static void main(String args[]) {
		RawBox integerBox = new RawBox();
		integerBox.add(45);
		
		int someInt = (Integer)integerBox.get();
		System.out.println("intero: " + someInt);
		
		String someString = (String)integerBox.get();
		System.out.println("stringa: " + someString);
		
		System.out.println("intero: " + someInt);
	}
	
}

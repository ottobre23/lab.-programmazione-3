package it.unimi.mat.generics.legacy;

import java.util.Collection;

public interface Assembly {

	@SuppressWarnings("unchecked")
	public Collection getParts();
	
}

package it.unimi.mat.generics.legacy.reinvented;

import it.unimi.mat.generics.legacy.Part;

import java.util.Collection;

public interface Assembly {

	Collection<Part> getParts();
	
}

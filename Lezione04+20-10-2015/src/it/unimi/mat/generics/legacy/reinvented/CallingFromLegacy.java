package it.unimi.mat.generics.legacy.reinvented;

import it.unimi.mat.generics.legacy.Part;

import java.util.ArrayList;
import java.util.Collection;

public class CallingFromLegacy {

	public class Blade implements Part {
		
	}
	
	public class Guillotine implements Part {
		
	}
	
	@SuppressWarnings({"rawtypes", "unchecked", "unused"})
	public static void main(String args[]) {
		Collection c = new ArrayList();
		
		CallingFromLegacy l = new CallingFromLegacy();
		c.add(l.new Guillotine());
		c.add(l.new Blade());
		Inventory.addAssembly("inv", c);
		Collection k = Inventory.getAssembly("inv").getParts();
	}
	
}

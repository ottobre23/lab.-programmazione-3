package it.unimi.mat.generics.legacy.reinvented;

import java.util.Collection;

public class Supertyping {

	public interface Sink<T> {
		void flush(T t);
	}
	
	public static <T> T writeAll(Collection<T> coll, Sink<? super T> snk) {
		T last = null;
		for(T t : coll) {
			last = t;
			snk.flush(t);
		}
		return last;
	}
	
	public static void main(String args[]) {
		Sink<Object> s = null;
		Collection<String> cs = null;
		@SuppressWarnings("unused")
		String str = writeAll(cs, s);
	}
}

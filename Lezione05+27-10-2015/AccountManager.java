
import it.unimi.mat.designpatterns.facade.CustomerFacade;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class AccountManager {

	private Shell shell;
	private final CustomerFacade cf;
	
	private final Text tFirst;
	private final Text tLast;
	private final Text tAddress;
	private final Text tCity;
	private final Text tState;
	private final Combo cCardType;
	private final Text tCardNumber;
	private final Text tExpDate;
	private final Label lResultDesc;
	
	public boolean isDisposed() {
		return shell.isDisposed();
	}

	public void open() {
		shell.open();
	}

	public void pack() {
		shell.pack();
	}

	public AccountManager(Display d) {
		cf = new CustomerFacade();
		
		shell = new Shell(d);
		GridLayout gl = new GridLayout();
		gl.numColumns = 2;
		shell.setLayout(gl);
		
		GridData gLabel = new GridData();
		gLabel.widthHint = 100;
		GridData gText = new GridData(GridData.FILL_HORIZONTAL);
		
		Label lFirst = new Label(shell, SWT.NONE);
		lFirst.setText("First Name");
		lFirst.setLayoutData(gLabel);
		tFirst = new Text(shell, SWT.NONE);
		tFirst.setLayoutData(gText);
		
		Label lLast = new Label(shell, SWT.NONE);
		lLast.setText("Last Name");
		lLast.setLayoutData(gLabel);
		tLast = new Text(shell, SWT.NONE);
		tLast.setLayoutData(gText);
		
		Label lAddress = new Label(shell, SWT.NONE);
		lAddress.setText("Address");
		lAddress.setLayoutData(gLabel);
		tAddress = new Text(shell, SWT.NONE);
		tAddress.setLayoutData(gText);
		
		Label lCity = new Label(shell, SWT.NONE);
		lCity.setText("City");
		lCity.setLayoutData(gLabel);
		tCity = new Text(shell, SWT.NONE);
		tCity.setLayoutData(gText);
		
		Label lState = new Label(shell, SWT.NONE);
		lState.setText("State");
		lState.setLayoutData(gLabel);
		tState = new Text(shell, SWT.NONE);
		tState.setLayoutData(gText);
		
		Label lCardType = new Label(shell, SWT.NONE);
		lCardType.setText("Card Type");
		lCardType.setLayoutData(gLabel);
		cCardType = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
		cCardType.setItems(new String[] {"Visa", "Discover", "Mastercard"});
		
		Label lCardNumber = new Label(shell, SWT.NONE);
		lCardNumber.setText("Card Number");
		lCardNumber.setLayoutData(gLabel);
		tCardNumber = new Text(shell, SWT.NONE);
		tCardNumber.setLayoutData(gText);
		
		Label lExpDate = new Label(shell, SWT.NONE);
		lExpDate.setText("Expiration Date");
		lExpDate.setLayoutData(gLabel);
		tExpDate = new Text(shell, SWT.NONE);
		tExpDate.setLayoutData(gText);
		
		Label lResult = new Label(shell, SWT.NONE);
		lResult.setText("Result");
		lResult.setLayoutData(gLabel);
		lResultDesc = new Label(shell, SWT.NONE);
		lResultDesc.setText("Click on Validate & Save Button");
		
		Button bValidateSave = new Button(shell, SWT.PUSH);
		bValidateSave.setText("Validate and Save");
		bValidateSave.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					cf.setFirstName(tFirst.getText());
					cf.setLastName(tLast.getText());
					cf.setAddress(tAddress.getText());
					cf.setCity(tCity.getText());
					cf.setState(tState.getText());
					cf.setCardType(cCardType.getText());
					cf.setCardNumber(tCardNumber.getText());
					cf.setCardExpDate(tExpDate.getText());
					
					boolean result = cf.saveCustomerData();
					if (result)
						lResultDesc.setText("Customer validated and saved");
					else
						lResultDesc.setText("Invalid customer data");
				}
			}
		);
		
		Button bExit = new Button(shell, SWT.PUSH);
		bExit.setText("Exit");
		bExit.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					shell.dispose();
				}
			}
		);

		
	}
	
	public static void main(String args[]) {
		Display display = new Display();
		AccountManager am = new AccountManager(display);
		am.open();
		am.pack();
		
		while(!am.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
	}
	
}


//import it.unimi.mat.designpatterns.classadapter.*;
import it.unimi.mat.designpatterns.objectadapter.*;

public class AdapterTester {
	public static void main(String args[]) {
		Customer c1 = new Customer("John Doe", "53 Tolouse St, New Orleans", "70130-1587", "LA", "US");
		System.out.println(c1);
		
		Customer c2 = new Customer("Jack Denn", "87 Ave, Edmonton", "AB T6G", "Alberta", "Canada");
		System.out.println(c2);
		
		Customer c3 = new Customer("Justin Marts", "12547 Strip, Las Vegas", "XXX", "NV", "US");
		System.out.println(c3);
	}
}

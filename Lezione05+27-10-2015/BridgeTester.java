
import it.unimi.mat.designpatterns.bridge.*;

public class BridgeTester {
	
	public static void main(String args[]) {
	
		MessageLogger logger = new ConsoleLogger();
		Message msg = new EncryptedMessage(logger);
		msg.log("A test message");
		
	}
}

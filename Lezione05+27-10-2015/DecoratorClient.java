
import it.unimi.mat.designpatterns.decorator.*;

public class DecoratorClient {
	
	public static void main(String args[]) {

		LoggerFactory factory = new LoggerFactory();
		Logger logger = factory.getLogger();
		
		HTMLLogger hLogger = new HTMLLogger(logger);
		hLogger.log("A message to log");
		
		EncryptLogger eLogger = new EncryptLogger(logger);
		eLogger.log("A message to log");
		
		EncryptLogger doubleLogger = new EncryptLogger(hLogger);
		doubleLogger.log("A message to log");
		
	}
}


import it.unimi.mat.designpatterns.facade.CustomerFacade;
import it.unimi.mat.designpatterns.facade.CreditCard;

public class FacadeTester {

	public static void main(String args[]) {
		CustomerFacade f = new CustomerFacade();
		f.setFirstName("John");
		f.setLastName("Doe");
		f.setAddress("2763 Oxford St");
		f.setCity("Miami");
		f.setState("FL");
		f.setCardType(CreditCard.VISA);
		f.setCardNumber("9573847293856102"); // aggiungere una cifra per rendere il dato non valido
		f.setCardExpDate("2010/10");
		
		boolean result = f.saveCustomerData();
		if(result) System.out.println("dati validi");
		else System.out.println("dati non validi");
	}
	
}

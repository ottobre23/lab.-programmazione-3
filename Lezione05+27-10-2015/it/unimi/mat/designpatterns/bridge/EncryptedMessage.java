package it.unimi.mat.designpatterns.bridge;

public class EncryptedMessage implements Message {

	private MessageLogger logger;
	
	public EncryptedMessage(MessageLogger logger) {
		this.logger = logger;
	}
	
	private String preProcess(String str) {
		str = str.substring(str.length() - 1) + str.substring(0, str.length() - 1);
		return str;
	}
	
	public void log(String msg) {
		logger.logMsg(preProcess(msg));
	}
	
}

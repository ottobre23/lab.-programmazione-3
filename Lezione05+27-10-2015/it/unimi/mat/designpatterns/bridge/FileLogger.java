package it.unimi.mat.designpatterns.bridge;

public class FileLogger implements MessageLogger {

	public void logMsg(String msg) {
		String fileName = "log.txt";
		System.out.println("[al file: " + fileName + "]: " + msg);
	}
	
}

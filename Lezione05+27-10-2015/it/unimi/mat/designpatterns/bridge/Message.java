package it.unimi.mat.designpatterns.bridge;

public interface Message {

	public void log(String msg);
	
}

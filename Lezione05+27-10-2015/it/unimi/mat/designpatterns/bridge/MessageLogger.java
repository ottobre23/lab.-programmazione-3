package it.unimi.mat.designpatterns.bridge;

public interface MessageLogger {

	public void logMsg(String msg);
	
}

package it.unimi.mat.designpatterns.bridge;

public class TextMessage implements Message {

	private MessageLogger logger;
	
	public TextMessage(MessageLogger logger) {
		this.logger = logger;
	}
	
	private String preProcess(String str) {
		return str;
	}
	
	public void log(String msg) {
		logger.logMsg(preProcess(msg));
	}
	
}

package it.unimi.mat.designpatterns.classadapter;

public interface AddressValidator {
	public boolean isValidAddress(String address, String zip, String state);
}

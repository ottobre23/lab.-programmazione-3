package it.unimi.mat.designpatterns.classadapter;

public class CAAddressAdapter extends CAAddress implements AddressValidator {

	public boolean isValidAddress(String address, String zip, String state) {
		return isValidCanadianAddr(address, zip, state);
	}
	
}

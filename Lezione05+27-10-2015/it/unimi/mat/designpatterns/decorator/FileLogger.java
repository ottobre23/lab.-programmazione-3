package it.unimi.mat.designpatterns.decorator;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;


public class FileLogger implements Logger {

	private static FileLogger logger;
	
	private FileLogger() {
		
	}
	
	public static FileLogger getFileLogger() {
		if(logger == null) {
			logger = new FileLogger();
		}
		return logger;
	}

	void writeToFile(String fileName, String msg) {
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(fileName, true));
			pw.println(msg);
			pw.close();
		} catch (IOException e) {
			System.err.println("Errore di I/O.");
		}
	}
	
	public synchronized void log(String msg) {
		writeToFile("log.txt", msg);

	}
	
}

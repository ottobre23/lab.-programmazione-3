package it.unimi.mat.designpatterns.decorator;

public class HTMLLogger extends LoggerDecorator {
	public HTMLLogger(Logger logger) {
		super(logger);
	}
	
	public void log(String msg) {
		msg = makeHTML(msg);
		logger.log(msg);
	}
	
	private String makeHTML(String msg) {
		msg = "<html>\n  <body>\n    <b>" + msg + "</b>\n  </body>\n</html>";
		return msg;
	}
}

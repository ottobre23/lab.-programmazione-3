package it.unimi.mat.designpatterns.decorator;

public interface Logger {

	public void log(String msg);
	
}

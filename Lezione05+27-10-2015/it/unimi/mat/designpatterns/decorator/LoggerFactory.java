package it.unimi.mat.designpatterns.decorator;


import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;


public class LoggerFactory {
	public boolean isFileLoggingEnabled() {
		String fileLoggingValue = null; 
		try {
			BufferedReader br = new BufferedReader(new FileReader("Logger.properties"));
			String line;
			while ( ( line = br.readLine() ) != null ) {
				int pos = line.indexOf('=');
				String property = line.substring(0, pos).trim();
				if( property.equals("FileLogging") ) {
					fileLoggingValue = line.substring( pos + 1 ).trim();
					break;
				}
			}
		} catch (IOException e) {
			System.err.println("Errore di I/O.");
		}
		if( fileLoggingValue == null )
			fileLoggingValue = "OFF";
		if( fileLoggingValue.equalsIgnoreCase( "ON" ) == true )
			return true;
		else return false;
	}
	
	public Logger getLogger() {
		if(isFileLoggingEnabled()) {
			return FileLogger.getFileLogger();
		} else {
			return new ConsoleLogger();
		}
	}
}

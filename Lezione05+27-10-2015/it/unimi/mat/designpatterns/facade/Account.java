package it.unimi.mat.designpatterns.facade;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;

public class Account {

	private String firstName;
	private String lastName;
	private final String ACCOUNT_DATA_FILE = "account.txt";
	
	public Account(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public boolean isValid() {
		return true;
	}
	
	public boolean save() {
		boolean result = false;
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(ACCOUNT_DATA_FILE, true));
			String dataLine = getLastName() + ", " + getFirstName();
			pw.println(dataLine);
			pw.close();
			result = true;
		} catch (IOException e) {
			System.err.println("Errore di I/O");
		}
		return result;
	}
	
	public String getFirstName() {	return firstName;	}
	
	public String getLastName() {	return lastName;	}
	
}

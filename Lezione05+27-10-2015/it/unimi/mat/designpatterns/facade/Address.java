package it.unimi.mat.designpatterns.facade;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;

public class Address {

	private String address;
	private String city;
	private String state;
	private final String ADDRESS_DATA_FILE = "address.txt";
	
	public Address(String address, String city, String state) {
		this.address = address;
		this.city = city;
		this.state = state;
	}
	
	public String getAddress() {	return address;	}
	public String getCity() {	return city;	}
	public String getState() {	return state;	}
	
	public boolean isValid() {
		return (getState().trim().length() <= 2);
	}
	
	public boolean save() {
		boolean result = false;
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(ADDRESS_DATA_FILE, true));
			String dataLine = getAddress() + ", " + getCity() + ", " + getState();
			pw.println(dataLine);
			pw.close();
			result = true;
		} catch (IOException e) {
			System.err.println("Errore di I/O");
		}
		return result;
	}
	
}

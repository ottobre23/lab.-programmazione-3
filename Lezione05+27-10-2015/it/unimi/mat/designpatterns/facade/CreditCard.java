package it.unimi.mat.designpatterns.facade;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;

public class CreditCard {

	private String cardType;
	private String cardNumber;
	private String cardExpDate;
	private static final String CC_DATA_FILE = "cc.txt";
	
	public static final String VISA = "Visa";
	public static final String DISCOVER = "Discover";
	public static final String MASTER = "Mastercard";
	
	public CreditCard(String cardType, String cardNumber, String cardExpDate) {
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.cardExpDate = cardExpDate;
	}
	
	public String getCardType() {	return cardType;	}
	public String getCardNumber() {	return cardNumber;	}
	public String getCardExpDate() {	return cardExpDate;	}
	
	public boolean isValid() {
		if(getCardType().equals(VISA))
			return (getCardNumber().trim().length() == 16);
		if(getCardType().equals(DISCOVER))
			return (getCardNumber().trim().length() == 15);
		if(getCardType().equals(MASTER))
			return (getCardNumber().trim().length() == 16);
		return false;
	}
	
	public boolean save() {
		boolean result = false;
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(CC_DATA_FILE, true));
			String dataLine = getCardType() + ", " + getCardNumber() + ", " + getCardExpDate();
			pw.println(dataLine);
			pw.close();
			result = true;
		} catch (IOException e) {
			System.err.println("Errore di I/O");
		}
		return result;
	}
	
}

package it.unimi.mat.designpatterns.facade;

public class CustomerFacade {

	private String address;
	private String city;
	private String state;
	private String cardType;
	private String cardNumber;
	private String cardExpDate;
	private String firstName;
	private String lastName;
	
	public void setAddress(String address) {	this.address = address;	}
	public void setCity(String city) {	this.city = city;	}
	public void setState(String state) {	this.state = state;	}
	public void setFirstName(String firstName) {	this.firstName = firstName;	}
	public void setLastName(String lastName) {	this.lastName = lastName;	}
	public void setCardType(String cardType) {	this.cardType = cardType;	}
	public void setCardNumber(String cardNumber) {	this.cardNumber = cardNumber;	}
	public void setCardExpDate(String cardExpDate) {	this.cardExpDate = cardExpDate;	}
	
	public boolean saveCustomerData() {
	
		boolean validData = true;		
		String errorMsg = "";
		
		Account objAccount = new Account(firstName, lastName);
		if(!objAccount.isValid()) {
			validData = false;
			errorMsg = "Invalid first name / last name";
		}
		
		Address objAddress = new Address(address, city, state);
		if(!objAddress.isValid()) {
			validData = false;
			errorMsg = "Invalid address / city / state";
		}
		
		CreditCard objCreditCard = new CreditCard(cardType, cardNumber, cardExpDate);
		if(!objCreditCard.isValid()) {
			validData = false;
			errorMsg = "Invalid credit card info";
		}
		
		if(!validData) {
			System.err.println(errorMsg);
			return false;
		}
		
		return (objAddress.save() && objAccount.save() && objCreditCard.save());
	}
	
}

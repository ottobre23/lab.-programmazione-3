package it.unimi.mat.designpatterns.objectadapter;

public abstract class AddressValidator {
	public abstract boolean isValidAddress(String address, String zip, String state);
}

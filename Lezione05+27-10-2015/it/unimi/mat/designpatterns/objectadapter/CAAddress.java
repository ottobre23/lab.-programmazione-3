package it.unimi.mat.designpatterns.objectadapter;

public class CAAddress {
	
	public boolean isValidCanadianAddr(String address, String pcode, String prvnc) {
		
		if(address.trim().length() < 15)
			return false;
		if(pcode.trim().length() != 6)
			return false;
		if(prvnc.trim().length() < 6)
			return false;
		
		return true;
		
	}

}

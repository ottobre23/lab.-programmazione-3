package it.unimi.mat.designpatterns.objectadapter;

public class CAAddressAdapter extends AddressValidator {

	private CAAddress objCAAddress;
	
	public CAAddressAdapter(CAAddress a) {	objCAAddress = a;	}
	
	public boolean isValidAddress(String address, String zip, String state) {
		return objCAAddress.isValidCanadianAddr(address, zip, state);
	}
	
}

package it.unimi.mat.designpatterns.objectadapter;

public class Customer {

	private static final String US = "US";
	private static final String CANADA = "Canada";
	private String address;
	private String name;
	private String zip, state, type;
	
	public Customer(String name, String address, String zip, String state, String type) {
		this.name = name;
		this.address = address;
		this.zip = zip;
		this.state = state;
		this.type = type;
	}
	
	private AddressValidator getValidator(String type) {
		AddressValidator validator = null;
		if(type.equals(US)) validator = new USAAddress();
		if(type.equals(CANADA)) validator = new CAAddressAdapter(new CAAddress());
		return validator;
	}
	
	public boolean isValidAddress() {
		AddressValidator validator = getValidator(type);
		return validator.isValidAddress(address, zip, state);
	}
	
	public String toString() {
		StringBuffer res = new StringBuffer(name);
		res.append("\n");
		res.append(address + " " + zip + " " + state + " (");
		if(!isValidAddress()) res.append("not ");
		res.append("valid)");
		
		return res.toString();
	}
	
}


import it.unimi.mat.designpattern.strategy.CaesarCypher;
import it.unimi.mat.designpattern.strategy.CodeBookCypher;
import it.unimi.mat.designpattern.strategy.EncryptLogger;
import it.unimi.mat.designpattern.strategy.SimpleEncryption;
import it.unimi.mat.designpattern.strategy.SubstitutionCypher;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class EncryptTester {
	
	public static void main(String args[]) {
		
		Display display = Display.getDefault();
		Shell shell = new Shell(display);
		GridLayout gl = new GridLayout();
		gl.numColumns = 2;
		shell.setLayout(gl);
			
		GridData gSpan = new GridData(GridData.FILL_HORIZONTAL);
		gSpan.horizontalSpan = 2;
		
		final Text tPlain = new Text(shell, SWT.NONE);
		tPlain.setLayoutData(gSpan);
		
		final Combo cAlgorithm = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
		cAlgorithm.setItems(new String[] {"Caesar cypher", "Codebook cypher", "Simple encryption", "Substitution cypher"});
		
		Button bEncrypt = new Button(shell, SWT.PUSH);
		bEncrypt.setText("Encrypt");
		
		final Label lEncoded = new Label(shell, SWT.NONE);
		lEncoded.setText("");
		lEncoded.setLayoutData(gSpan);
		
		final EncryptLogger logger = new EncryptLogger();
		
		bEncrypt.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					
					String alg = cAlgorithm.getText();
					if(alg.equals("Caesar cypher"))
						logger.setEncryptionStrategy(new CaesarCypher());
					else if(alg.equals("Codebook cypher"))
						logger.setEncryptionStrategy(new CodeBookCypher());
					else if(alg.equals("Simple encryption"))
						logger.setEncryptionStrategy(new SimpleEncryption());
					else logger.setEncryptionStrategy(new SubstitutionCypher());
					
					lEncoded.setText(logger.encrypt(tPlain.getText()));
				}
			}
		);
		
		shell.open();
		shell.pack();
		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
		 
	}

}


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import it.unimi.mat.designpattern.memento.Application;

public class MementoTester {
	
	public static void main(String[] args) {
		final Application a = new Application(9);
		a.setState(1);
		a.setState(2);
		a.setState(3);
		
		System.out.println(a.getState());
		a.undo();
		System.out.println(a.getState());
		a.undo();
		System.out.println(a.getState());
		a.undo();
		System.out.println(a.getState());
		a.undo();
		System.out.println(a.getState());
		
		Display display = Display.getDefault();
		Shell shell = new Shell(display);
		GridLayout gl = new GridLayout();
		gl.numColumns = 2;
		shell.setLayout(gl);
			
		GridData gFill = new GridData(GridData.FILL_HORIZONTAL);
		
		Label lStateDesc = new Label(shell, SWT.NONE);
		lStateDesc.setText("Stato corrente");
		final Label lState = new Label(shell, SWT.NONE);
		lState.setText("" + a.getState());
		lState.setLayoutData(gFill);
		
		Label lNewState = new Label(shell, SWT.NONE);
		lNewState.setText("Nuovo stato");
		final Text tNewState = new Text(shell, SWT.NONE);
		tNewState.setLayoutData(gFill);
		
		Button bChange = new Button(shell, SWT.PUSH);
		bChange.setText("Cambia stato");
		bChange.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					int state = Integer.parseInt(tNewState.getText());
					a.setState(state);
					lState.setText("" + a.getState());
				}
			}
		);
		
		Button bUndo = new Button(shell, SWT.PUSH);
		bUndo.setText("Undo");
		bUndo.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					a.undo();
					lState.setText("" + a.getState());
				}
			}
		);
		
		shell.open();
		shell.pack();
		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
		
	}

}

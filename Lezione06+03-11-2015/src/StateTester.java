
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import it.unimi.mat.designpattern.state.BusinessAccount;

public class StateTester {
	
	public static void main(String args[]) {
		final BusinessAccount account = new BusinessAccount("new account");
		account.deposit(2000.0);
		account.withdraw(1500.0);
		account.withdraw(700.0);
		account.withdraw(1200.0);
		account.deposit(1200.0);
		account.deposit(3000.0);
		account.deposit(10.0);
		
		
		
		Display display = Display.getDefault();
		Shell shell = new Shell(display);
		GridLayout gl = new GridLayout();
		gl.numColumns = 2;
		shell.setLayout(gl);
			
		GridData gFill = new GridData(GridData.FILL_HORIZONTAL);
		
		Label lBalanceDesc = new Label(shell, SWT.NONE);
		lBalanceDesc.setText("Saldo");
		final Label lBalance = new Label(shell, SWT.NONE);
		lBalance.setText("" + account.getBalance());
		lBalance.setLayoutData(gFill);
		
		Label lStateDesc = new Label(shell, SWT.NONE);
		lStateDesc.setText("Stato");
		final Label lState = new Label(shell, SWT.NONE);
		lState.setText("" + account.getState());
		lState.setLayoutData(gFill);
		
		Label lAmount = new Label(shell, SWT.NONE);
		lAmount.setText("Importo");
		final Text tAmount = new Text(shell, SWT.NONE);
		tAmount.setLayoutData(gFill);
		
		Button bDeposit = new Button(shell, SWT.PUSH);
		bDeposit.setText("Versa");
		bDeposit.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					account.deposit(Double.parseDouble(tAmount.getText()));
					lState.setText("" + account.getState());
					lBalance.setText("" + account.getBalance());
				}
			}
		);
		
		Button bWithdraw = new Button(shell, SWT.PUSH);
		bWithdraw.setText("Preleva");
		bWithdraw.addSelectionListener(
				new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						account.withdraw(Double.parseDouble(tAmount.getText()));
						lState.setText("" + account.getState());
						lBalance.setText("" + account.getBalance());
					}
				}
			);
		
		
		

		

		
		shell.open();
		shell.pack();
		
		while(!shell.isDisposed()) {
			if(!display.readAndDispatch()) 
				display.sleep();
		}
		
		display.dispose();
		 
	}

}

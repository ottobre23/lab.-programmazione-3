
import it.unimi.mat.designpattern.observer.ReportManager;
import it.unimi.mat.designpattern.observer.MonthlyReport;

public class TestObserver {

	public static void main(String args[]) {
		ReportManager manager = new ReportManager("r1");
		
		MonthlyReport m1 = new MonthlyReport("m1", manager);
		MonthlyReport m2 = new MonthlyReport("m2", manager);
		
		manager.setDepartment("Dipartimento 1");
		manager.notifyObservers();
		
		manager.setDepartment("Dipartimento 2");
		manager.notifyObservers();
		
		manager.unRegister(m1);
		MonthlyReport m3 = new MonthlyReport("m3", manager);
		
		manager.setDepartment("Dipartimento 3");
		manager.notifyObservers();
		
		manager.unRegister(m2);
		manager.unRegister(m3);
		manager.notifyObservers();
		
	}
	
}

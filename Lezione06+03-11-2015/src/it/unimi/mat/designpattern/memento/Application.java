package it.unimi.mat.designpattern.memento;

public class Application {

	private int state;
	private MementoHandler mh;
	
	public Application(int state) {
		mh = new MementoHandler();
		this.state = state;
	}
	
	public int getState() {
		return state;
	}
	
	public void setState(int state) {
		mh.setMemento(new Memento(this));
		this.state = state;
	}
	
	public void undo() {
		Memento m = mh.getMemento();
		if(m == null)
			System.out.println("Nothing to undo.");
		else this.state = m.getState();
		return;
	}

}

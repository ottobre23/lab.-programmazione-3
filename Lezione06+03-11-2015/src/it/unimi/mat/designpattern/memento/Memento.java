package it.unimi.mat.designpattern.memento;

public class Memento {

	private final int state;
	
	public Memento(Application a) {
		this.state = a.getState();
	}
	
	public int getState() {
		return state;
	}
	
}

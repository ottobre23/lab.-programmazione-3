package it.unimi.mat.designpattern.memento;

import java.util.List;
import java.util.ArrayList;

public class MementoHandler {

	private List<Memento> mementos;
	
	public MementoHandler() {
		mementos = new ArrayList<Memento>();
	}
	
	public void setMemento(Memento m) {
		mementos.add(m);
	}
	
	public Memento getMemento() {
		if(mementos.isEmpty())
			return null;
		return mementos.remove(mementos.size()-1);
	}
	
}

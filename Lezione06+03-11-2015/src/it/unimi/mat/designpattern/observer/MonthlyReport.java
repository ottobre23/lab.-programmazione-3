package it.unimi.mat.designpattern.observer;

public class MonthlyReport implements Observer {

	private ReportManager report;
	private String id;
	
	public MonthlyReport(String id, ReportManager report) {
		this.id = id;
		this.report = report;
		report.register(this);
	}
	
	public void refreshData(Observable o) {
		if(o == report)
			System.out.println("[" + this + "]: l'oggetto (" + report + ") ha inviato (" + report.getDepartment() + ")");
	}
	
	public String toString() {
		return id;
	}
	
}

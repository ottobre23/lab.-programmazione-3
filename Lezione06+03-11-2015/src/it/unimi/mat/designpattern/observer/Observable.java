package it.unimi.mat.designpattern.observer;

public interface Observable {

	public void register(Observer o);
	public void unRegister(Observer o);
	public void notifyObservers();
	
}

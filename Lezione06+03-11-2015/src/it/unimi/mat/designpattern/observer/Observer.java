package it.unimi.mat.designpattern.observer;

public interface Observer {

	public void refreshData(Observable o);
	
}

package it.unimi.mat.designpattern.observer;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class ReportManager implements Observable {

	private List<Observer> observerList;
	private String id;
	private String department;
	
	public ReportManager(String id) {
		observerList = new ArrayList<Observer>();
		this.id = id;
	}
	
	public void notifyObservers() {
		for(Observer o : observerList)
			o.refreshData(this);
	}
	
	public void notifyObservers2() {
		for(Iterator<Observer> i = observerList.iterator(); i.hasNext(); )
			i.next().refreshData(this);
	}
	
	public void register(Observer o) {
		observerList.add(o);
	}
	
	public void unRegister(Observer o) {
		observerList.remove(o);
	}
	
	public String getDepartment() {
		return department;
	}
	
	public void setDepartment(String department) {
		this.department = department;
	}
	
	public String toString() {
		return id;
	}
	
}

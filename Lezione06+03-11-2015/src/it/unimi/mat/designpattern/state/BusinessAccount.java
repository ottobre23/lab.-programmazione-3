package it.unimi.mat.designpattern.state;

public class BusinessAccount {

	public static final double MIN_BALANCE = 2000.0;
	public static final double OVERDRAW_LIMIT = -1000.0;
	public static final double TRANS_FEE_NORMAL = 2.0;
	public static final double TRANS_FEE_OVERDRAW = 5.0;
	public static final String ERR_OVERDRAW_LIMIT_EXCEED =
		"Error: Transaction cannot be processed: overdraw limit exceeded.";
	
	private State objState;
	private String accountNumber;
	private double balance;
	
	public BusinessAccount(String accountNumber) {
		this.accountNumber = accountNumber;
		objState = State.getInitialState(this);
	}
	
	public void setState(State newState) {
		this.objState = newState;
	}
	
	public State getState() {
		return objState;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public boolean deposit(double amount) {
		return objState.deposit(amount);
	}
	
	public boolean withdraw(double amount) {
		return objState.withdraw(amount);
	}
	
	
	
}

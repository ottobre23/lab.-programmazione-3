package it.unimi.mat.designpattern.state;

public class NoTransactionFeeState extends State {
	
	public NoTransactionFeeState(BusinessAccount context) {
		super(context);
	}
	
	public NoTransactionFeeState(State s) {
		super(s);
	}

	public boolean deposit(double amount) {
		BusinessAccount context = getContext();
		
		double balance = context.getBalance();
		context.setBalance(balance + amount);
		transitionState();
		System.out.println("An amount " + amount + " is deposited.");
		return true;
	}
	
	public boolean withdraw(double amount) {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		if(balance - amount > BusinessAccount.OVERDRAW_LIMIT) {
			super.withdraw(amount);
			return true;
		}
		else {
			System.out.println(BusinessAccount.ERR_OVERDRAW_LIMIT_EXCEED);
			return false;
		}
	}
	
	public State transitionState() {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		
		if(balance < 0) {
			context.setState(new OverDrawnState(this));
		}
		else {
			if(balance < BusinessAccount.MIN_BALANCE) {
				context.setState(new TransactionFeeState(this));
			}
		}
		return context.getState();
	}
	
	public String toString() {
		return "No transaction free state";
	}
}

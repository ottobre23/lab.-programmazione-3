package it.unimi.mat.designpattern.state;

public class OverDrawnState extends State {

	public OverDrawnState(State s) {
		super(s);
		// Add specific behaivour (e.g., send e-mail to client)
	}
	
	public OverDrawnState(BusinessAccount context) {
		super(context);
		// Add specific behaivour (e.g., send e-mail to client)
	}
	
	public State transitionState() {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		
		if(balance >= BusinessAccount.MIN_BALANCE)
			context.setState(new NoTransactionFeeState(this));
		else if(balance >= 0)
			context.setState(new TransactionFeeState(this));
		return context.getState();
	}
	
	public boolean deposit(double amount) {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		
		context.setBalance(balance - BusinessAccount.TRANS_FEE_OVERDRAW);
		System.out.println("Transaction fee was charged due to: account overdrawn.");
		return super.deposit(amount);
	}
	
	public boolean withdraw(double amount) {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		
		if(balance - BusinessAccount.TRANS_FEE_OVERDRAW -  amount > BusinessAccount.OVERDRAW_LIMIT) {
			context.setBalance(balance - BusinessAccount.TRANS_FEE_OVERDRAW);
			System.out.println("Transaction fee was charged due to: account overdrawn.");
			return super.withdraw(amount);
		}
		
		System.out.println(BusinessAccount.ERR_OVERDRAW_LIMIT_EXCEED);
		return false;
	}
	
	public String toString() {
		return "Overdrawn state";
	}
	
}

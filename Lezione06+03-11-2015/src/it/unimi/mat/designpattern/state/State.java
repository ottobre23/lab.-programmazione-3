package it.unimi.mat.designpattern.state;

public class State {

	private BusinessAccount context;
	
	public State(BusinessAccount context) {
		this.context = context;
	}
	
	public State(State s) {
		setContext(s.getContext());
	}
	
	public BusinessAccount getContext() {
		return this.context;
	}
	
	public void setContext(BusinessAccount context) {
		this.context = context;
	}
	
	public State transitionState() {
		return null;
	}
	
	public static State getInitialState(BusinessAccount context) {
		return new NoTransactionFeeState(context);
	}
	
	public boolean deposit(double amount) {
		BusinessAccount context = getContext();
		
		double balance = context.getBalance();
		context.setBalance(balance + amount);
		transitionState();
		System.out.println("An amount " + amount + " is deposited (balance is now " + context.getBalance() + ").");
		return true;
	}
	public boolean withdraw(double amount) {
		BusinessAccount context = getContext();
		
		double balance = context.getBalance();
		context.setBalance(balance - amount);
		transitionState();
		System.out.println("An amount " + amount + " is withdrawn (balance is now " + context.getBalance() + ").");
		return true;
	}
	
}

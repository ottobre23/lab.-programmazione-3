package it.unimi.mat.designpattern.state;

public class TransactionFeeState extends State {

	public TransactionFeeState(State s) {
		super(s);
	}
	
	public TransactionFeeState(BusinessAccount context) {
		super(context);
	}
	
	public State transitionState() {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		if(balance < 0)
			context.setState(new OverDrawnState(this));
		else if (balance >= BusinessAccount.MIN_BALANCE)
			context.setState(new NoTransactionFeeState(this));
		
		return context.getState();
	}
	
	public boolean deposit(double amount) {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		
		context.setBalance(balance - BusinessAccount.TRANS_FEE_NORMAL);
		System.out.println("Transaction fee was charged due to: balance less than minimal balance");
		return super.deposit(amount);
	}
	
	public boolean withdraw(double amount) {
		BusinessAccount context = getContext();
		double balance = context.getBalance();
		
		if(balance - BusinessAccount.TRANS_FEE_NORMAL - amount > BusinessAccount.OVERDRAW_LIMIT) {
			context.setBalance(balance - BusinessAccount.TRANS_FEE_NORMAL);
			System.out.println("Transaction fee was charged due to: balance less than minimal balance");
			return super.withdraw(amount);
		}
		else {
			System.out.println("Transaction fee was charged due to: balance less than minimal balance");
			return false;
		}
	}
	
	public String toString() {
		return "Transaction free state";
	}
	
}

package it.unimi.mat.designpattern.strategy;

import java.util.HashMap;
import java.util.StringTokenizer;

public class CodeBookCypher implements EncryptionStrategy {

	private HashMap<String, String> codeContents;
	
	public CodeBookCypher() {
		codeContents = new HashMap<String, String>();
		populateEntries();
	}
	
	private void populateEntries() {
		codeContents.put("this", "Design");
		codeContents.put("is", "Patterns");
		codeContents.put("a", "are");
		codeContents.put("true", "really");
		codeContents.put("statement", "useful");
	}
	
	public String encrypt(String input) {
		StringBuffer result = new StringBuffer();
		StringTokenizer st = new StringTokenizer(input, " ");
		while(st.hasMoreTokens())
			result.append(codeContents.get(st.nextToken()) + " ");
		
		return result.toString();
	}
	
}

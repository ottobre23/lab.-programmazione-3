package it.unimi.mat.designpattern.strategy;

public class EncryptLogger {
	
	private EncryptionStrategy strategy;
	
	public EncryptLogger() {
		setEncryptionStrategy(new SimpleEncryption());
	}
	
	public void log(String s) {
		System.out.println(encrypt(s));
	}
	
	public String encrypt(String s) {
		return strategy.encrypt(s);
	}
	
	public void setEncryptionStrategy(EncryptionStrategy strategy) {
		this.strategy = strategy;
	}

}

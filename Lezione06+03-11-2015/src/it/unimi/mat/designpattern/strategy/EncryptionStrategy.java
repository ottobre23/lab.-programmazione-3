package it.unimi.mat.designpattern.strategy;

public interface EncryptionStrategy {

	public String encrypt(String input);
	
}

package it.unimi.mat.designpattern.strategy;

public class SimpleEncryption implements EncryptionStrategy {

	public String encrypt(String input) {
		return input.charAt(input.length()-1) + input.substring(0, input.length()-1);
	}
	
}

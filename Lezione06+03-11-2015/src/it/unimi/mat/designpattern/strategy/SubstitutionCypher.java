package it.unimi.mat.designpattern.strategy;

public class SubstitutionCypher implements EncryptionStrategy {
	char encrypted[] = {'m', 'n', 'o', 'p', 'q', 'r', 'a', 'b', 'c', 'd', 'e',
			'f', 'g', 'h', 'i', 'j', 'k', 'l', 'y', 'z', 's', 't', 'u', 'v', 'w', 'x', };
	
	public String encrypt(String input) {
		StringBuffer result = new StringBuffer();
		for(int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			result.append(Character.isLetter(c) && Character.isLowerCase(c) ? encrypted[c - 'a'] : c);
		}
		return result.toString();
	}
}

import it.unimi.mat.mvc.TemperatureController;
import it.unimi.mat.mvc.TemperatureModel;


public class MVCTester {

	public static void main(String[] args) {
		new TemperatureController(new TemperatureModel()).go();
	}
	
}

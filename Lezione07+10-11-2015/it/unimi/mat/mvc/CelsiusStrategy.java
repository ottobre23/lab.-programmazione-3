package it.unimi.mat.mvc;

public class CelsiusStrategy implements TemperatureStrategy {

	public static TemperatureStrategy instance = new CelsiusStrategy();
	
	private CelsiusStrategy() {
		
	}
	
	public double fromCelsius(double celsius) {
		return celsius;
	}
	
	public double toCelsius(double t) {
		return t;
	}
	
	public double defaultValue() {
		return 0.0;
	}
	
	public double minimumValue() {
		return -40.0;
	}
	
	public double maximumValue() {
		return 120.0;
	}
	
}

package it.unimi.mat.mvc;

public class FahrenheitStrategy implements TemperatureStrategy {

	private FahrenheitStrategy() {
		
	}
	
	public static TemperatureStrategy instance = new FahrenheitStrategy();
	
	public double fromCelsius(double celsius) {
		return 32 + 9*celsius/5.0;
	}
	
	public double toCelsius(double t) {
		return (t-32) * 5 / 9;
	}
	
	public double defaultValue() {
		return 32.0;
	}
	
	public double minimumValue() {
		return -40.0;
	}
	
	public double maximumValue() {
		return 240.0;
	}
}

package it.unimi.mat.mvc;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class GaugeView implements Observer {

	TemperatureGauge gauge;
	TemperatureCanvas canvas;
	TemperatureModel model;
	TemperatureStrategy strategy;
	
	public GaugeView(TemperatureModel model, TemperatureStrategy strategy,
			TemperatureController controller, String label) {
		Shell shell = new Shell(controller.display);
		
		Label l = new Label(shell, SWT.NONE);
		l.setText(label);
		
		this.model = model;
		this.strategy = strategy;
		
		gauge = new TemperatureGauge((int)strategy.minimumValue(), 
				(int)strategy.maximumValue());
		gauge.set((int)strategy.defaultValue());
		canvas = new TemperatureCanvas(gauge, shell);
		
		canvas.addMouseListener(controller.gaugeListener(this));
		canvas.addMouseMoveListener(controller.gaugeMoveListener(this));
	}
	
	public void update(Observable obs, Object o) {
		double temp = model.getValue(strategy);
		gauge.set((int)temp);
		canvas.paint();
	}
	
}

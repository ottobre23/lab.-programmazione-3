package it.unimi.mat.mvc;

public class KelvinStrategy implements TemperatureStrategy {

	public static TemperatureStrategy instance = new KelvinStrategy();
	
	private KelvinStrategy() {
		
	}
	
	 public double defaultValue() {
		 return 273.15;
	 }
	 
	 public double fromCelsius(double celsius) {
		 return celsius + 273.15;
	 }
	 
	 public double toCelsius(double temp) {
		 return temp - 273.15;
	 }
	 
	 public double minimumValue() {
		 return 0;
	 }
	 
	 public double maximumValue() {
		 return 400.0;
	 }
	
}

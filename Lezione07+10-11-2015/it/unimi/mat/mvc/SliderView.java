package it.unimi.mat.mvc;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;

public class SliderView implements Observer {
	
	TemperatureModel model;
	TemperatureController controller;
	TemperatureStrategy strategy;
	
	Scale s;

	public SliderView(TemperatureModel model, 
			TemperatureStrategy strategy,
			TemperatureController controller) {
		this.model = model;
		this.strategy = strategy;
		this.controller = controller;

		Shell shell = new Shell(controller.display);
		
		RowLayout l = new RowLayout();
		shell.setLayout(l);
		
		s = new Scale(shell, SWT.HORIZONTAL);
		s.setMinimum((int)strategy.minimumValue());
		s.setMaximum((int)strategy.maximumValue());
		s.setSelection((int)strategy.defaultValue());
		
		s.addSelectionListener(controller.sliderListener(this));
		
		shell.pack();
		shell.open();
	}
	
	public void update(Observable obs, Object obj) {
		double temp = model.getValue(strategy);
		s.setSelection((int)temp);
	}
	
}

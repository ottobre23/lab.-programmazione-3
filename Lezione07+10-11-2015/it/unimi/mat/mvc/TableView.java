package it.unimi.mat.mvc;

import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class TableView {

	TemperatureModel model;
	TemperatureStrategy strategy;
	//TemperatureController controller;
	
	Table t;
	
	public TableView(TemperatureModel model,
			TemperatureStrategy strategy,
			TemperatureController controller) {
		this.model = model;
		this.strategy = strategy;
		
		Shell shell = new Shell(controller.display);
		GridLayout g = new GridLayout();
		g.numColumns = 1;
		shell.setLayout(g);
		
		GridData gd = new GridData(GridData.FILL_BOTH);
		
		Button b = new Button(shell, SWT.PUSH);
		b.setText("Registra");
		b.setLayoutData(gd);
		
		t = new Table(shell, SWT.BORDER);
		t.setLayoutData(gd);
		t.setLinesVisible(true);
		TableColumn time = new TableColumn(t, SWT.LEFT);
		time.setWidth(250);
		TableColumn temperature = new TableColumn(t, SWT.RIGHT);
		temperature.setWidth(40);
		

		final TemperatureModel m = model;
		final TemperatureStrategy s = strategy;
		b.addSelectionListener(
			new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					TableItem item = new TableItem(t, SWT.NONE);
					item.setText(new String[] {new Date().toString(), Double.toString(m.getValue(s))});
					}
			});
		
		
		shell.pack();
		shell.open();
		
		
	}
	
}

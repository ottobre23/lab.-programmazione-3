package it.unimi.mat.mvc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Shell;

public class TemperatureCanvas extends Canvas {

	private TemperatureGauge gauge;
	private GC gc;
	
	static final int width = 20;
	static final int top = 20;
	static final int left = 50;
	static final int height = 200;
	
	public TemperatureCanvas(TemperatureGauge gauge, Shell s) {
		super(s, SWT.NONE);
		this.setSize(200, 280);
		this.setLocation(100, 100);
		s.pack();
		s.open();
		
		this.gauge = gauge;
		gc = new GC(this);
		paint();
	}
	
	public void paint() {
		Color black = this.getDisplay().getSystemColor(SWT.COLOR_BLACK);
		Color red = this.getDisplay().getSystemColor(SWT.COLOR_RED);
		Color white = this.getDisplay().getSystemColor(SWT.COLOR_WHITE);
		Color gray = this.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
	
		gc.setForeground(black);
		gc.setBackground(gray);
		gc.fillRectangle(0, 0, 200, 280);
		
		gc.setBackground(red);
		gc.drawRectangle(left, top, width, height);
		gc.fillOval(left-width/2, top+height-width/3, width*2, width*2);
		gc.drawOval(left-width/2, top+height-width/3, width*2, width*2);
		
		gc.setBackground(white);
		gc.fillRectangle(left+1, top+1, width-1, height-1);
		
		long redtop = height*(gauge.get() - gauge.getMax())/(gauge.getMin() - gauge.getMax());
		
		gc.setBackground(gray);
		gc.drawText(Integer.toString(gauge.get()), 10, top+(int)redtop);
		
		gc.setBackground(red);
		
		gc.fillRectangle(left+1, top+(int)redtop, width-1, height-(int)redtop);
		
		
	}
	
}

package it.unimi.mat.mvc;

import java.util.Observer;

import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;



public class TemperatureController {

	final Display display;
	private final TemperatureModel model;
	
	public TemperatureController(TemperatureModel model) {
		this.model = model;
		this.display = Display.getDefault();
		
		addView(new TextualView(model, CelsiusStrategy.instance, this, "Celsius"));
		addView(new TextualView(model, FahrenheitStrategy.instance, this, "Fahrenheit"));
		addView(new GaugeView(model, CelsiusStrategy.instance, this, "Celsius"));
		addView(new GaugeView(model, FahrenheitStrategy.instance, this, "Fahrenheit"));
		addView(new GaugeView(model, KelvinStrategy.instance, this, "Kelvin"));
		addView(new SliderView(model, KelvinStrategy.instance, this));
		addView(new SliderView(model, CelsiusStrategy.instance, this));
		addView(new SliderView(model, FahrenheitStrategy.instance, this));
		
		new TableView(model, CelsiusStrategy.instance, this);
	}
	
	private void addView(Observer view) {
		model.addObserver(view);
	}
	
	SelectionListener raiseListener(TextualView tv) {
		final TemperatureStrategy s = tv.getStrategy();
		
		return new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				model.setValue(model.getValue(s) + 1.0, s);
			}
		};
	}
	
	SelectionListener lowerListener(TextualView tv) {
		final TemperatureStrategy s = tv.getStrategy();
		
		return new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				model.setValue(model.getValue(s) - 1.0, s);
			}
		};
	}
	
	
	KeyListener textListener(TextualView tv) {
		final TemperatureStrategy s = tv.getStrategy();
		KeyListener textListener = new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				double result = s.defaultValue();
				Text t = (Text)e.widget;
				try {
					result = Double.valueOf(t.getText());
				} catch(NumberFormatException ex) {
					t.setText(Double.toString(s.defaultValue()));
				}
				model.setValue(result, s);
			}
		};
		return textListener;
	}
	
	SelectionListener sliderListener(SliderView sv) {
		final TemperatureStrategy str = sv.strategy;
		return new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Scale s = (Scale)e.getSource();
				model.setValue(s.getSelection(), str);
			}
		};
	}
	
	MouseListener gaugeListener(GaugeView g) {
		final GaugeView gv = g;
		final TemperatureStrategy s = g.strategy;
		return new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				double t = gv.gauge.getMax() + 
					((double)e.y - TemperatureCanvas.top) / 
					TemperatureCanvas.height * 
					(gv.gauge.getMin() - gv.gauge.getMax());
				model.setValue(t, s);
			}
		};
	}
	
	MouseMoveListener gaugeMoveListener(GaugeView g) {
		final GaugeView gv = g;
		final TemperatureStrategy s = g.strategy;
		return new MouseMoveListener() {
			public void mouseMove(MouseEvent e) {
				if((e.stateMask & SWT.BUTTON1) != 0) {
					double t = gv.gauge.getMax() +
					    ((double)e.y - TemperatureCanvas.top) /
					    TemperatureCanvas.height *
					    (gv.gauge.getMin() - gv.gauge.getMax());
					model.setValue(t, s);
				}
			}
		};
	}
	
	public void go() {
		while ( model.countObservers() > 0 ) {
			if ( !display.readAndDispatch() )
				display.sleep();
		}
		display.dispose();
	}
	
}

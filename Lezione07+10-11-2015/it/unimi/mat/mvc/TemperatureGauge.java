package it.unimi.mat.mvc;

public class TemperatureGauge {

	private int min;
	private int max;
	private int current;
	
	public TemperatureGauge(int min, int max) {
		this.min = min;
		this.max = max;
		this.current = 0;
	}
	
	public int get() {
		return current;
	}
	
	public void set(int c) {
		current = c;
	}
	
	public int getMin() {
		return min;
	}
	
	public int getMax() {
		return max;
	}
	
}

package it.unimi.mat.mvc;

import java.util.Observable;

public class TemperatureModel extends Observable {

	private double temperatureC = 0.0;
	
	public double getValue(TemperatureStrategy s) {
		return s.fromCelsius(temperatureC);
	}
	
	private double truncate(double value, int d) {
		double power10 = Math.pow(10, d);
		return Math.floor(value*power10) / power10;
	}
	
	public void setValue(double t, TemperatureStrategy s) {
		temperatureC = truncate(s.toCelsius(t), 2);
		//temperatureC = s.toCelsius(t);
		setChanged();
		notifyObservers();
	}
	

	
}

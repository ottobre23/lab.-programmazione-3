package it.unimi.mat.mvc;

public interface TemperatureStrategy {

	public double fromCelsius(double celsius);
	
	public double toCelsius(double other);
	
	public double defaultValue();
	
	public double minimumValue();
	
	public double maximumValue();
	
}

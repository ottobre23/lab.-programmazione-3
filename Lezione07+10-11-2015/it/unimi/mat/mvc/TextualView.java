package it.unimi.mat.mvc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import java.util.Observer;
import java.util.Observable;

public class TextualView implements Observer {

	private TemperatureModel model;
	private TemperatureController controller;
	private String label;
	private Text t;
	
	private TemperatureStrategy strategy;
	
	public TextualView(TemperatureModel mod, 
			TemperatureStrategy str, 
			TemperatureController cont, String lab) {
		model = mod;
		strategy = str;
		controller = cont;
		label = lab;
		
		Shell shell;
		Button bRaise;
		Button bLower;
		
		shell = new Shell(controller.display);
		shell.setText(this.label);
		
		GridLayout g = new GridLayout();
		g.numColumns = 2;
		shell.setLayout(g);
		
		Label l = new Label(shell, SWT.NONE);
		l.setText(label);
		GridData dS = new GridData(GridData.FILL_HORIZONTAL);
		dS.horizontalSpan = 2;
		l.setLayoutData(dS);
		
		t = new Text(shell, SWT.NONE);
		t.setLayoutData(dS);
		
		bRaise = new Button(shell, SWT.PUSH);
		bRaise.setText("Raise");
		bLower = new Button(shell, SWT.PUSH);
		bLower.setText("Lower");
		GridData dB = new GridData(GridData.FILL_HORIZONTAL);
		bRaise.setLayoutData(dB);
		bLower.setLayoutData(dB);
		
		bRaise.addSelectionListener(controller.raiseListener(this));
		bLower.addSelectionListener(controller.lowerListener(this));
		t.setText(Double.toString(strategy.defaultValue()));
		t.addKeyListener(controller.textListener(this));
		
		shell.pack();
		shell.open();
	}
	
	TemperatureStrategy getStrategy() {
		return this.strategy;
	}
	
	public void update(Observable obs, Object obj) {
		t.setText(Double.toString(model.getValue(strategy)));
	}
	
}

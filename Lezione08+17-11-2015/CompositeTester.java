import it.unimi.mat.designpatterns.composite.*;


public class CompositeTester {

	public static void main(String args[]) {
		FileSystemComponent mainFolder = new DirComponent("Year2000");
		FileSystemComponent subFolder1 = new DirComponent("Jan");
		FileSystemComponent subFolder2 = new DirComponent("Feb");
		FileSystemComponent folder1File1 = new FileComponent("Jan1DataFile", 1000);
		FileSystemComponent folder1File2 = new FileComponent("Jan2DataFile", 2000);
		FileSystemComponent folder2File1 = new FileComponent("Feb1DataFile", 3000);
		FileSystemComponent folder2File2 = new FileComponent("Feb2DataFile", 4000);
		try {
			mainFolder.addComponent(subFolder1);
			mainFolder.addComponent(subFolder2);
			subFolder1.addComponent(folder1File1);
			subFolder1.addComponent(folder1File2);
			subFolder2.addComponent(folder2File1);
			subFolder2.addComponent(folder2File2);
		} catch (CompositeException e) {
			// Questa eccezione deve essere gestita per contratto
			// ma in realt� il codice non la lancer� MAI.
			throw new UnsupportedOperationException("Qualcosa � andato storto...");
		}
		
		System.out.println("Main folder size " + mainFolder.getComponentSize());
		System.out.println("Sub Folder1 size " + subFolder1.getComponentSize());
		System.out.println("File1 in Folder1 size " + folder1File1.getComponentSize());
	}	
	
}

import it.unimi.mat.designpatterns.composite.CompositeException;
import it.unimi.mat.designpatterns.composite.DirComponent;
import it.unimi.mat.designpatterns.composite.FileComponent;
import it.unimi.mat.designpatterns.composite.FileSystemComponent;

import it.unimi.mat.designpatterns.visitor.PrintFileSystemVisitor;
import it.unimi.mat.designpatterns.visitor.MaxFileVisitor;


public class FileSystemVisitorTester {

	public static void main(String args[]) {
		FileSystemComponent mainFolder = new DirComponent("Year2000");
		FileSystemComponent subFolder1 = new DirComponent("Jan");
		FileSystemComponent subFolder2 = new DirComponent("Feb");
		FileSystemComponent folder1File1 = new FileComponent("Jan1DataFile", 1000);
		FileSystemComponent folder1File2 = new FileComponent("Jan2DataFile", 2000);
		FileSystemComponent folder2File1 = new FileComponent("Feb1DataFile", 3000);
		FileSystemComponent folder2File2 = new FileComponent("Feb2DataFile", 4000);
		try {
			mainFolder.addComponent(subFolder1);
			mainFolder.addComponent(subFolder2);
			subFolder1.addComponent(folder1File1);
			subFolder1.addComponent(folder1File2);
			subFolder2.addComponent(folder2File1);
			subFolder2.addComponent(folder2File2);
		} catch (CompositeException e) {
			//
		}
		
		PrintFileSystemVisitor v = new PrintFileSystemVisitor();
		mainFolder.accept(v);
		
		MaxFileVisitor mv = new MaxFileVisitor();
		mainFolder.accept(mv);
		System.out.println("\n\n" + mv.getMaxSize());
	}
	
}

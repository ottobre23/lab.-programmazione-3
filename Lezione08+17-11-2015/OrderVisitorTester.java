import it.unimi.mat.designpatterns.visitor.*;

import java.util.ArrayList;
import java.util.List;


public class OrderVisitorTester {

	public static void main(String[] args) {
		
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(new CaliforniaOrder(1000, 10));
		orderList.add(new NonCaliforniaOrder(2000));
		orderList.add(new OverseasOrder(3000, 20));
		
		TotalOrderVisitor v = new TotalOrderVisitor();
		System.out.println(v.doVisit(orderList));
		
		MaxOrderVisitor mv = new MaxOrderVisitor();
		System.out.println(mv.doVisit(orderList));
		
	}
	
}

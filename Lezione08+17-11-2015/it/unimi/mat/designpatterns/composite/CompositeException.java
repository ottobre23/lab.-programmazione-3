package it.unimi.mat.designpatterns.composite;

public class CompositeException extends Exception {

	private static final long serialVersionUID = -8158288200610679903L;

	public CompositeException(String s) {
		super(s);
	}
	
}

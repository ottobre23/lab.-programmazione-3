package it.unimi.mat.designpatterns.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.unimi.mat.designpatterns.visitor.FileSystemVisitor;

public class DirComponent extends FileSystemComponent implements Iterable<FileSystemComponent> {
	
	List<FileSystemComponent> dirContents = new ArrayList<FileSystemComponent>();

	public DirComponent(String name) {
		super(name);
	}
	
	public void addComponent(FileSystemComponent c) throws CompositeException {
		dirContents.add(c);
	}
	
	public FileSystemComponent getComponent(int i) throws CompositeException {
		return dirContents.get(i);
	}
	
	public Iterator<FileSystemComponent> iterator() {
		return dirContents.iterator();
	}

	public long getComponentSize() {
		long totalSize = 0;
		
		for(FileSystemComponent c  : dirContents)
			totalSize += c.getComponentSize();
		
		return totalSize;
	}
	
	public void accept(FileSystemVisitor v) {
		v.visit(this);
	}
}

package it.unimi.mat.designpatterns.composite;

import it.unimi.mat.designpatterns.visitor.FileSystemVisitor;

public class FileComponent extends FileSystemComponent {

	private long size;

	public FileComponent(String name, long size) {
		super(name);
		this.size = size;
	}

	public long getComponentSize() {
		return size;
	}
	
	public void accept(FileSystemVisitor v) {
		v.visit(this);
	}
	
}

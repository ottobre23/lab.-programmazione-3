package it.unimi.mat.designpatterns.composite;

import it.unimi.mat.designpatterns.visitor.FileSystemVisitor;

public abstract class FileSystemComponent {

	String name;
	
	public FileSystemComponent(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addComponent(FileSystemComponent c) throws CompositeException {
		throw new CompositeException("Operation not supported");
	}
	
	public FileSystemComponent getComponent(int i) throws CompositeException {
		throw new CompositeException("Operation not supported");
	}
	
	public abstract long getComponentSize();
	public abstract void accept(FileSystemVisitor v);
	
}

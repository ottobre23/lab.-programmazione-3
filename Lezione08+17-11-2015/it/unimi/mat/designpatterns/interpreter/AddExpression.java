package it.unimi.mat.designpatterns.interpreter;

public class AddExpression extends NonTerminalExpression {

	public AddExpression(Expression leftNode, Expression rightNode) {
		super(leftNode, rightNode);
	}

	public int evaluate(Context c) {
		return getLeftNode().evaluate(c) + getRightNode().evaluate(c);
	}
	
	String getSymbol() {
		return "+";
	}
	
}

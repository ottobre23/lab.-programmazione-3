package it.unimi.mat.designpatterns.interpreter;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Calculator {

	private String expression;
	private Map<Character, Integer> operators;
	private Context ctx;
	
	public Calculator() {
		operators = new HashMap<Character, Integer>();
		operators.put('+', 1);
		operators.put('-', 1);
		operators.put('/', 3);
		operators.put('*', 2);
		operators.put('(', 0);
	}
	
	public void setContext(Context ctx) {
		this.ctx = ctx;
	}
	
	public void setExpression(String expression) {
		this.expression = expression;
	}
	
	public String getPostfixExpression() {
		return infixToPostfix(expression);
	}
	
	private String infixToPostfix(String str) {
		Stack<Character> s = new Stack<Character>();
		String pfExpr = "";
		char tempChr = '\0';
		String expr = str.trim();
		for(char c : expr.toCharArray()) {
			if((!isOperator(c)) && (c != '(') && (c != ')'))
				pfExpr += c;
			if(c == '(')
				s.push(c);
			if(c == ')') {
				tempChr = s.pop();
				while(tempChr != '(') {
					pfExpr += tempChr;
					tempChr = s.pop();
				}
				tempChr = '\0';
			}
			if(isOperator(c)) {
				if(!s.isEmpty()) {
					tempChr = s.pop();
					int val1 = operators.get(tempChr);
					int val2 = operators.get(c);
					while(val1 >= val2) {
						pfExpr += tempChr;
						val1 = -100;
						if(!s.isEmpty()) {
							tempChr = s.pop();
							val1 = operators.get(tempChr);
						}
					}
					if((val1 < val2) && (val1 != -100))
						s.push(tempChr);
				}
				s.push(c);
			}
		}
		while(!s.isEmpty()) {
			tempChr = s.pop();
			pfExpr += tempChr;
		}
		return pfExpr;
	}
	
	private boolean isOperator(char c) {
		return ((c == '+') || (c == '-') || (c == '*'));
	}
	
	public int evaluate() {
		String postfix = this.infixToPostfix(this.expression);
		Expression e = this.buildTree(postfix);
		return e.evaluate(this.ctx);
	}
	
	private Expression buildTree(String expr) {
		Stack<Expression> s = new Stack<Expression>();
		for(char c : expr.toCharArray()) {
			if(!isOperator(c)) {
				Expression e = new TerminalExpression(c);
				s.push(e);
			}
			else {
				Expression right = s.pop();
				Expression left = s.pop();
				Expression n = getNonTerminalExpression(c, left, right);
				s.push(n);
			}
		}
		return s.pop();
	}
	
	private Expression getNonTerminalExpression(char c, Expression left, Expression right) {
		Expression e = null;
		switch(c) {
		case '+': e = new AddExpression(left, right);		break;
		case '-': e = new SubtractExpression(left, right);	break;
		case '*': e = new MultiplyExpression(left, right);	break;
		}
		return e;
	}
	
	public String traverseExpression() {
		String postfix = this.infixToPostfix(this.expression);
		Expression e = this.buildTree(postfix);
		return e.toString();
	}
	
	
	public static void main(String[] args) {
		Calculator calc = new Calculator();
		Context ctx = new Context();
		String expr = "(a+b)*(c-d)";
		calc.setExpression(expr);
		calc.setContext(ctx);
		System.out.println(calc.getPostfixExpression());
		System.out.println(calc.evaluate());
		System.out.println(calc.traverseExpression());
		
		
		}
	
}

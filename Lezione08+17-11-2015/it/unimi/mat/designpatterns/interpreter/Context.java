package it.unimi.mat.designpatterns.interpreter;

import java.util.HashMap;
import java.util.Map;

public class Context {

	private Map<Character, Integer> varList = new HashMap<Character, Integer>();
	
	public Context() {
		initialize();
	}
	
	public void assign(char var, int value) {
		varList.put(var, value);
	}
	
	public int getValue(char var) {
		return varList.get(var);
	}
	
	private void initialize() {
		assign('a', 20);
		assign('b', 40);
		assign('c', 30);
		assign('d', 10);
	}
	
}

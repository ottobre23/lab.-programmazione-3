package it.unimi.mat.designpatterns.interpreter;

public interface Expression {

	public int evaluate(Context c);
	
}

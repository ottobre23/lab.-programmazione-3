package it.unimi.mat.designpatterns.interpreter;

public abstract class NonTerminalExpression implements Expression {

	private Expression leftNode;
	private Expression rightNode;
	private TraversalPolicy trav = TraversalPolicy.IN_ORDER;
	
	public enum TraversalPolicy {
		PREORDER, POSTORDER, IN_ORDER
	}
	
	public NonTerminalExpression(Expression leftNode, Expression rightNode) {
		this.leftNode = leftNode;
		this.rightNode = rightNode;
	}
	
	public Expression getLeftNode() {
		return leftNode;
	}
	
	public Expression getRightNode() {
		return rightNode;
	}
	
	public String toString() {
		String s = null;
		
		switch(trav) {
		case PREORDER: s = "(" + getSymbol() + getLeftNode().toString() + getRightNode().toString() + ")";	break;
		case POSTORDER: s = "(" + getLeftNode().toString() + getRightNode().toString() + getSymbol() + ")";	break;
		case IN_ORDER: s = "(" + getLeftNode().toString() + getSymbol() + getRightNode().toString() + ")";	break;
		}
		return s;
	}
	
	abstract String getSymbol();


}

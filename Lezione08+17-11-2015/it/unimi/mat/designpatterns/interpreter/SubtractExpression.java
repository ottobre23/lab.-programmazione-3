package it.unimi.mat.designpatterns.interpreter;

public class SubtractExpression extends NonTerminalExpression {

	public SubtractExpression(Expression leftNode, Expression rightNode) {
		super(leftNode, rightNode);
	}
	
	public int evaluate(Context c) {
		return getLeftNode().evaluate(c) - getRightNode().evaluate(c);
	}

	String getSymbol() {
		return "-";
	}
	
}

package it.unimi.mat.designpatterns.interpreter;

import it.unimi.mat.designpatterns.interpreter.NonTerminalExpression.TraversalPolicy;

public class TerminalExpression implements Expression {

	private char var;
	
	public TerminalExpression(char var) {
		this.var = var;
	}
	
	public int evaluate(Context c) {
		return c.getValue(var);
	}
	
	public String toString() {
		return "" + var;
	}
	
	public String toString(TraversalPolicy p) {
		return toString();
	}
	
}

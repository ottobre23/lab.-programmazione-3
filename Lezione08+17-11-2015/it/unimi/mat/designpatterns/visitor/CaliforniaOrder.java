package it.unimi.mat.designpatterns.visitor;

public class CaliforniaOrder implements Order {

	private double orderAmount;
	private double additionalTax;
	
	public CaliforniaOrder(double orderAmount, double additionalTax) {
		this.orderAmount = orderAmount;
		this.additionalTax = additionalTax;
	}
	
	public double getOrderAmount() {
		return orderAmount;
	}
	
	public double getAdditionalTax() {
		return additionalTax;
	}
	
	public void accept(OrderVisitor v) {
		v.visit(this);
	}
	
}

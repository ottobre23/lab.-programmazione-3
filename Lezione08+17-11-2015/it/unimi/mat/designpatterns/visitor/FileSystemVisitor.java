package it.unimi.mat.designpatterns.visitor;

import it.unimi.mat.designpatterns.composite.FileComponent;
import it.unimi.mat.designpatterns.composite.DirComponent;

public interface FileSystemVisitor {

	public void visit(FileComponent fc);
	public void visit(DirComponent fc);
	
}

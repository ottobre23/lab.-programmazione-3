package it.unimi.mat.designpatterns.visitor;

import it.unimi.mat.designpatterns.composite.DirComponent;
import it.unimi.mat.designpatterns.composite.FileComponent;
import it.unimi.mat.designpatterns.composite.FileSystemComponent;

import java.util.Set;
import java.util.HashSet;

public class MaxFileVisitor implements FileSystemVisitor {

	private long maxSize;
	private Set<FileSystemComponent> visited;
	
	public MaxFileVisitor() {
		maxSize = 0;
		visited = new HashSet<FileSystemComponent>();
	}

	public void visit(FileComponent fc) {
		if(!visited.contains(fc)) {
			long size = fc.getComponentSize();
			if(size > maxSize) maxSize = size;
			visited.add(fc);
		}
	}
	
	public void visit(DirComponent dc) {
		if(!visited.contains(dc)) {
			for(FileSystemComponent c : dc)
				c.accept(this);
			visited.add(dc);
		}
	}
	
	public long getMaxSize() {
		return maxSize;
	}
}

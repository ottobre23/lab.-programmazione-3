package it.unimi.mat.designpatterns.visitor;

import java.util.Collection;

public class MaxOrderVisitor implements OrderVisitor {

	private double maxOrder;

	public void visit(NonCaliforniaOrder nco) {
		double amount = nco.getOrderAmount();
		if(amount > maxOrder) maxOrder = amount;
	}

	public void visit(CaliforniaOrder co) {
		double amount = co.getOrderAmount() + co.getAdditionalTax();
		if(amount > maxOrder) maxOrder = amount;
	}

	public void visit(OverseasOrder oo) {
		double amount = oo.getOrderAmount() + oo.getAdditionalSH();
		if(amount > maxOrder) maxOrder = amount;
	}
	
	public double doVisit(Collection<Order> orderColl) {
		maxOrder = -1.0;
		for(Order o : orderColl)
			o.accept(this);
		return maxOrder;
	}	
	
}

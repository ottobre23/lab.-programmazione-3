package it.unimi.mat.designpatterns.visitor;

public class NonCaliforniaOrder implements Order {

	private double orderAmount;
	
	public NonCaliforniaOrder(double orderAmount) {
		this.orderAmount = orderAmount;
	}
	
	public double getOrderAmount() {
		return orderAmount;
	}
	
	public void accept(OrderVisitor v) {
		v.visit(this);
	}	
	
}

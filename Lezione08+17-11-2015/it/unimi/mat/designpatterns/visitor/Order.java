package it.unimi.mat.designpatterns.visitor;

public interface Order {
	public void accept(OrderVisitor v);
}

package it.unimi.mat.designpatterns.visitor;

public interface OrderVisitor {

	public void visit(NonCaliforniaOrder nco);
	public void visit(CaliforniaOrder co);
	public void visit(OverseasOrder oo);
	
}

package it.unimi.mat.designpatterns.visitor;

public class OverseasOrder implements Order {

	private double orderAmount;
	private double additionalSH;
	
	public OverseasOrder(double orderAmount, double additionalSH) {
		this.orderAmount = orderAmount;
		this.additionalSH = additionalSH;
	}
	
	public double getOrderAmount() {
		return orderAmount;
	}
	
	public double getAdditionalSH() {
		return additionalSH;
	}
	
	public void accept(OrderVisitor v) {
		v.visit(this);
	}	
	
}

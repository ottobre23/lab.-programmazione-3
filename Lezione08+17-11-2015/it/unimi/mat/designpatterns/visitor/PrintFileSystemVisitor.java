package it.unimi.mat.designpatterns.visitor;

import it.unimi.mat.designpatterns.composite.*;

public class PrintFileSystemVisitor implements FileSystemVisitor {
	
	private final static int INDENT_SIZE = 2;
	
	private int indent;
	
	public PrintFileSystemVisitor() {
		indent = 0;
	}
	
	private void printIndentation() {
//		for(int i=0; i<indent*INDENT_SIZE; i++)
//			System.out.print(" ");
		
		for(int i=0; i<indent-1; i++) {
			System.out.print("|");
			for(int j=0; j<INDENT_SIZE-1; j++)
				System.out.print(" ");
		}
		if(indent>0) {
			System.out.print("|");
			for(int j=0; j<INDENT_SIZE-1; j++)
				System.out.print("-");
		}
		
	}

	public void visit(FileComponent fc) {
		printIndentation();
		System.out.println(fc.getName());
	}
	
	public void visit(DirComponent dc) {
		printIndentation();
		System.out.println(dc.getName());
		indent += 1;
		for(FileSystemComponent c : dc) {
			c.accept(this);
		}
		indent -= 1;
	}
	
}

package it.unimi.mat.designpatterns.visitor;

import java.util.Collection;

public class TotalOrderVisitor implements OrderVisitor {

	private double orderTotal;

	public void visit(NonCaliforniaOrder nco) {
		orderTotal += nco.getOrderAmount();
	}

	public void visit(CaliforniaOrder co) {
		orderTotal += co.getOrderAmount() + co.getAdditionalTax();
	}

	public void visit(OverseasOrder oo) {
		orderTotal += oo.getOrderAmount() + oo.getAdditionalSH();
	}
	
	public double doVisit(Collection<Order> orderColl) {
		orderTotal = 0.0;
		for(Order o : orderColl)
			o.accept(this);

		return orderTotal;
	}	
	
}

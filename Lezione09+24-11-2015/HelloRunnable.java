

public class HelloRunnable implements Runnable{

	private String message;
	
	public HelloRunnable(String message) {
		this.message = message;
	}
	
	public void run() {
		System.out.println(message);
	}
	
	public static void main(String[] args) {
		for(int i=0; i<10; i++)
			(new Thread(new HelloRunnable(""+i))).start();
		
	}
	
}

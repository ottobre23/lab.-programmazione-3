

public class HelloThread extends Thread {

	private String message;
	
	public HelloThread(String message) {
		this.message = message;
	}
	
	public void run () {
		System.out.println(message);
	}
	
	public static void main(String[] args) {
		for(int i=0; i<10; i++)
			(new HelloThread(i+"")).start();
	}
	
}



public class SimpleThreads {

	static void threadMessage(String message) {
		String threadName = Thread.currentThread().getName();
		System.out.println(threadName + ": " + message);
	}
	
	private static class MessageLoop implements Runnable {
		public void run() {
			String importantInfo[] = {"La nebbia agli irti colli",
					"piovigginando sale",
					"e sotto al maestrale",
					"urla e biancheggia il mare"
					};
			
			try {
				for(String m : importantInfo) {
					Thread.sleep(4000);
					threadMessage(m);
				}
			} catch(InterruptedException e) {
				threadMessage("Non si interrompe una poesia!");
			}
			
			// in alternativa all'attesa: Thread.yield()
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		long patience = 100 * 60 * 60;
		if(args.length > 0) {
			try {
				patience = Long.parseLong(args[0]) * 1000;
			} catch(NumberFormatException e) {
				System.err.println("Il primo argomento, se presente, deve essere un intero");
				System.exit(1);
			}
		}
		
		threadMessage("Ora racconto una poesia:");
		long startTime = System.currentTimeMillis();
		Thread t = new Thread(new MessageLoop());
		t.start();
		
		while(t.isAlive()) {
			threadMessage("stiamo aspettando...");
			t.join(1000);
			if(((System.currentTimeMillis() - startTime) > patience) && t.isAlive()) {
				threadMessage("e mo' basta!");
				t.interrupt();
				t.join();
			}
		}
		threadMessage("E' finita!");
	}
}
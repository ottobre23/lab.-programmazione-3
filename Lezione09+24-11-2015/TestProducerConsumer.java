
import it.unimi.mat.concurrency.*;

public class TestProducerConsumer {

	public static void main(String[] args) {
		Drop drop = new Drop();
		(new Thread(new Producer(drop))).start();
		(new Thread(new Consumer(drop))).start();
	}
	
}

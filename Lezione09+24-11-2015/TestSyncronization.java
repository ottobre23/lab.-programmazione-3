
import it.unimi.mat.concurrency.Counter;

public class TestSyncronization {

	public static void main(String[] args) throws InterruptedException {
		
		int count = 1000;
		int zero=0, pos=0, neg=0;
		
		for(int x=0; x<100; x++) {
			Counter.setCount(0);
			Thread inc = new Thread(new Counter(true, count));
			Thread dec = new Thread(new Counter(false, count));
			//Provate a usare SynchronizedCounter
			
			inc.start();
			dec.start();
			
			try {
				inc.join();
				dec.join();
			} catch(InterruptedException e) {
				System.out.println("Qualcuno mi ha bloccato.");
			}
			
			if(Counter.getCount() == 0) zero++;
			if(Counter.getCount() > 0) pos++;
			if(Counter.getCount() < 0) neg++;
		}
		
		System.out.println("zero: " + zero + "; positive: " + pos + "; negative: " + neg);
		
	}
	
}

package it.unimi.mat.concurrency;

import java.util.Random;

public class Consumer implements Runnable {
	
	private Drop drop;
	
	public Consumer(Drop drop) {
		this.drop = drop;
	}
	
	public void run() {
		Random r = new Random();
		for(String message = drop.take(); !message.equals("DONE"); message = drop.take()) {
			System.out.println(message);
			try {
				Thread.sleep(r.nextInt(500));
			} catch (InterruptedException e) { }
		}
	}

}

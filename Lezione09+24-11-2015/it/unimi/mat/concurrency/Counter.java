package it.unimi.mat.concurrency;

public class Counter implements Runnable {

	protected static int count = 0;
	private boolean increment;
	private int numCount;
	
	public Counter(boolean increment, int numCount) {
		this.increment = increment;
		this.numCount = numCount;
	}
	
	public void run() {
		for(int i=0; i<numCount; i++) {
			if(increment)
				increment();
			else
				decrement();
		}
	}
	
	// rendere il metodo sincronizzato non funziona
	public void increment() {
		count++;
	}
	
	// rendere il metodo sincronizzato non funziona
	public void decrement() {
		count--;
	}
	
	public static int getCount() {
		return count;
	}
	
	public static void setCount(int count) {
		Counter.count = count;
	}
	
}

package it.unimi.mat.concurrency;

public class Deadlock {

	static class Friend {
		private final String name;
		
		public Friend(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public synchronized void bow(Friend bower) {
			System.out.println(name + ": Salve, " + bower.getName() + "!");
			bower.bowBack(this);
		}
		
		public synchronized void bowBack(Friend bower) {
			System.out.println(name + ": Ma salve a lei, " + bower.getName() + "!");
		}
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");

		Thread a = new Thread(
				new Runnable(){
					public void run() { alphonse.bow(gaston); }
				}
		);
		a.start();
		
		Thread g= new Thread(
				new Runnable(){
					public void run() { gaston.bow(alphonse); }
				}
		);
		g.start();
		
		a.join();
		g.join();
		
		System.out.println("Fine.");
	}
	
}

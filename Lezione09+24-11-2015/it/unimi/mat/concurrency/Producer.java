package it.unimi.mat.concurrency;

import java.util.Random;

public class Producer implements Runnable {
	
	private Drop drop;
	
	public Producer(Drop drop) {
		this.drop = drop;
	}
	
	public void run() {
		String importantInfo[] = {
				"La nebbia agli irti colli",
				"piovigginando sale",
				"e sotto al maestrale",
				"urla e biancheggia il mare"
		};
		Random r = new Random();
		
		for(String s : importantInfo) {
			drop.put(s);
			try {
				Thread.sleep(r.nextInt(500));
			} catch (InterruptedException e) { }
		}
		drop.put("DONE");
	}
	
}
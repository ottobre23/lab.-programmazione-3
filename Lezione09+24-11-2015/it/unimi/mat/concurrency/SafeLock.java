package it.unimi.mat.concurrency;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SafeLock {
	
	static class Friend {
	
		private final String name;
		private final Lock lock = new ReentrantLock();
		
		public Friend(String name) {
			this.name = name;
		}
	
		public boolean impendingBow(Friend bower) {
			boolean myLock = false;
			boolean yourLock = false;
			
			try {
				myLock = lock.tryLock();
				yourLock = bower.lock.tryLock();
			} finally {
				if(!(myLock && yourLock)) {
					if(myLock) lock.unlock();
					if(yourLock) bower.lock.unlock();
				}
			}
			return myLock && yourLock;
		}
		
		public void bow(Friend bower) {
			if(impendingBow(bower)) {
				try{
					System.out.println(name + ": " + bower.name + " mi ha fatto un inchino");
					bower.bowBack(this);
				} finally {
					lock.unlock();
					bower.lock.unlock();
				}
			}
			else {
				System.out.println(name + ": " + bower.name + "stava per inchinarsi, ma ha visto che mi stavo gia' inchinando io");
			}	
		}
		
		public void bowBack(Friend bower) {
			System.out.println(name + ": " + bower.name + " ha risposto al mio inchino");
		}
	}
	
	static class BowLoop implements Runnable {
		private Friend bower;
		private Friend bowee;
		
		public BowLoop(Friend bower, Friend bowee) {
			this.bower = bower;
			this.bowee = bowee;
		}
		
		public void run() {
			Random r = new Random();
			while(true) {
				try {
					Thread.sleep(r.nextInt(10));
				} catch(InterruptedException e) {
					
				}
				bowee.bow(bower);
			}
		}
	}
	
	public static void main(String[] args) {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		
		new Thread(new BowLoop(alphonse, gaston)).start();
		new Thread(new BowLoop(gaston, alphonse)).start();
	}

}

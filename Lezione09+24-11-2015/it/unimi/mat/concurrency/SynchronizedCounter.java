package it.unimi.mat.concurrency;

public class SynchronizedCounter extends Counter {

	public SynchronizedCounter(boolean increment, int numCount) {
		super(increment, numCount);
	}
	
	public void increment() {
		synchronized(Counter.class) {
			count++;
		}
	}
	
	public void decrement() {
		synchronized(Counter.class) {
			count--;
		}
	}	
	
}

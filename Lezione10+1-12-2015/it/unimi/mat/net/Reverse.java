package it.unimi.mat.net;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Reverse {

	public static void main(String[] args) throws Exception {

		String stringToReverse = URLEncoder.encode(args[0], "UTF-8");
		URL url = new URL("http://127.0.0.1/cgi-bin/backward");
		URLConnection connection = url.openConnection();
		connection.setDoOutput(true);
		
		OutputStreamWriter out = new OutputStreamWriter(
				connection.getOutputStream());
		out.write("string=" + stringToReverse);
		out.close();
		
		BufferedReader in = new BufferedReader(
				new InputStreamReader(connection.getInputStream()));
		
		String inputLine;
		
		while((inputLine = in.readLine()) != null)
			System.out.println(inputLine);
		
		in.close();
		
		
	}

}
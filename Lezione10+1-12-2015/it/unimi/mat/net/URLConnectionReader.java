package it.unimi.mat.net;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionReader {

	public static void main(String[] args) throws Exception {
			
//		URL google = new URL("http://www.google.com/");
//		URLConnection gc = google.openConnection();
//		BufferedReader in = new BufferedReader(
//				new InputStreamReader(gc.getInputStream()));
//		String inputLine;
//		
//		while((inputLine = in.readLine()) != null)
//			System.out.println(inputLine);
//		
//		in.close();
		
		URL url = new URL("http://www.google.com/search?q=SEI2");
		URLConnection con = url.openConnection();
		con.setRequestProperty( "User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0; H010818)" );
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		
		String inputLine;
		
		while((inputLine = in.readLine()) != null)
			System.out.println(inputLine);
		
		in.close();
	}

}
package it.unimi.mat.net;


import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class URLManagement {
	
	public static void printURLInformation(URL u) {
		System.out.println("\nprotocol = " + u.getProtocol());
		System.out.println("authority = " + u.getAuthority());
		System.out.println("host = " + u.getHost());
		System.out.println("port = " + u.getPort());
		System.out.println("path = " + u.getPath());
		System.out.println("query = " + u.getQuery());
		System.out.println("filename = " + u.getFile());
		System.out.println("ref = " + u.getRef() + "\n");
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		try {
			URL unimi = new URL("http://www.unimi.it");
			URL chiedove = new URL(unimi, "/chiedove");
			URL chiedove2 = new URL("http", "www.unimi.it", "/chiedove");
			URL chiedove3 = new URL("http", "www.unimi.it", 80, "/chiedove");
			
			URL malformed = new URL("http://foo.com/hello world");
			System.out.println(malformed);
			
			URI uri = new URI("http", "foo.com", "/hello world/", "");
			URL wellformed = uri.toURL();
			System.out.println(wellformed);
			
			printURLInformation(chiedove);
			printURLInformation(malformed);
			printURLInformation(wellformed);
			
			URL lungo = new URL("http://java.sun.com:80/docs/books/tutorial/index.html?name=networking#DOWNLOADING");
			printURLInformation(lungo);
			
			
			
		} catch(MalformedURLException e) { e.printStackTrace(); }
		catch(URISyntaxException e) { e.printStackTrace(); }
	}

}

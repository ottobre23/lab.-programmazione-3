package it.unimi.mat.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URI;

@SuppressWarnings("unused")

public class URLReader {

	public static void main(String[] args) {
		try {
			URL url = new URL("http://homes.dsi.unimi.it/~malchiod/P3");
			//URL url = new URL("http://www.42r2342.com/");
			//URL url = new URL("http://127.0.0.1/ifts/visite al sito.php");
			//URL url = new URI("http", "127.0.0.1", "/ifts/visite al sito.php", "").toURL();
		
			BufferedReader in = new BufferedReader(
					new InputStreamReader(url.openStream()));
			
			String inputLine;
			while((inputLine = in.readLine()) != null)
				System.out.println(inputLine);
			
		} catch (MalformedURLException e) { e.printStackTrace(); }
		
		catch (IOException e) { e.printStackTrace(); }
		
		//catch (URISyntaxException e) { e.printStackTrace(); }
		
	}

}

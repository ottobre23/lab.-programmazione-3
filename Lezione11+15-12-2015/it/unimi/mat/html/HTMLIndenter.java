package it.unimi.mat.html;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;


public class HTMLIndenter {
	
	private static int indent = 0;
	
	private static String spaces() {
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<indent; i++)
			sb.append(' ');
		
		return sb.toString();
	}

	public static void main(String[] args) {

		HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback() {
			public void handleText(char[] data, int pos) {
				//if(data[0] == '>') return;
				indent++;
				System.out.print(spaces());
				System.out.println(data);
				indent--;
			}
			
			public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
				indent++;
				System.out.print(spaces());
				System.out.print("<" + t.toString());
				
				for(Enumeration<?> names = a.getAttributeNames(); names.hasMoreElements() ;) {
					Object attr = names.nextElement();
					System.out.print(" " + attr + " = '");
					System.out.print(a.getAttribute(attr) + "'");
					if(names.hasMoreElements())
						System.out.print(" ");
				}
				
				System.out.println(">");

			}
			
			public void handleEndTag(HTML.Tag t, int pos) {
				System.out.print(spaces());
				System.out.println("</" + t.toString() + ">");
				indent--;
			}
			
			public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
				System.out.print(spaces());
				System.out.print("<" + t.toString() + " ");
				for(Enumeration<?> names = a.getAttributeNames(); names.hasMoreElements() ;) {
					Object attr = names.nextElement();
					System.out.print(" " + attr + " = '");
					System.out.print(a.getAttribute(attr) + "'");
					if(names.hasMoreElements())
						System.out.print(" ");
				}
				System.out.println("/>");
			}
		};
		
		try {
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2");
			URL url = new URL("http://localhost/ifts/sei2/malformato.html");
			BufferedReader html = new BufferedReader(new InputStreamReader(url.openStream()));
			new ParserDelegator().parse(html, callback, true);
		} catch (MalformedURLException e) { e.printStackTrace(); }
		catch (IOException e) { e.printStackTrace(); }
		
	}
	
}

package it.unimi.mat.html;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class SimpleParser {
	
	static HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback() {
		public void handleText(char[] data, int pos) {
			System.out.println(data);
		}
		
		public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
			System.out.print("Start: " + t + " ");
				
			
			for(Enumeration<?> e = a.getAttributeNames(); e.hasMoreElements(); ) {
				HTML.Attribute attr = (HTML.Attribute)e.nextElement();
				System.out.print(attr + " = \"");
				System.out.print(a.getAttribute(attr) + "\" ");
			}
			System.out.println();
			
			
		}
		
		public void handleEndTag(HTML.Tag t, int pos) {
			System.out.println("End: " + t);
		}
		
		public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
			System.out.println("Tag: " + t);
		}
	};

	public static void main(String args[]) {
		try {
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2/laboratorio/link-tree.html");
			URL url = new URL("http://localhost/ifts/sei2/malformato.html");
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2/index.php");
			BufferedReader r = new BufferedReader(new InputStreamReader(url.openStream()));
			new ParserDelegator().parse(r, callback, true);
		} catch(MalformedURLException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}

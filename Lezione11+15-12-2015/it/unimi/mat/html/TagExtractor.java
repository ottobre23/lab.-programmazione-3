package it.unimi.mat.html;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class TagExtractor {
	
	static HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback() {
		
		public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
			if( t.equals(HTML.Tag.A) ) {
				String href = (String)a.getAttribute(HTML.Attribute.HREF);
				if(href != null)
					System.out.println(href);
			}
		}
	};

	public static void main(String args[]) {
		try {
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2/laboratorio/link-tree.html");
			URL url = new URL("http://localhost/ifts/sei2/malformato.html");
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2/index.php");
			BufferedReader r = new BufferedReader(new InputStreamReader(url.openStream()));
			new ParserDelegator().parse(r, callback, true);
		} catch(MalformedURLException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}

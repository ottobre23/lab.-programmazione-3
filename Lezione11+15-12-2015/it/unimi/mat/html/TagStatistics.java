package it.unimi.mat.html;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.HashMap;

public class TagStatistics {
	
	private Map<HTML.Tag, Integer> occurrences;
	private URL url;
	private HTMLEditorKit.ParserCallback callback;
	
	public TagStatistics(URL u) {
		url = u;
		occurrences = new HashMap<HTML.Tag, Integer>();
		
		callback = new HTMLEditorKit.ParserCallback() {
			
			public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
				Integer num = occurrences.get(t);
				occurrences.put(t, (num == null) ? 1 : num+1);
			}
			
			public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
				Integer num = occurrences.get(t);
				occurrences.put(t, (num == null) ? 1 : num+1);
			}
		};
	}
	
	public void process() {
		BufferedReader r;
		try {
			r = new BufferedReader(new InputStreamReader(url.openStream()));
			new ParserDelegator().parse(r, callback, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void printResults() {
		for(Map.Entry<HTML.Tag, Integer> e : occurrences.entrySet())
			System.out.println(e.getKey() + ": " + e.getValue());
	}
	
	public static void main(String args[]) {
		try {
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2/laboratorio/link-tree.html");
			//URL url = new URL("http://homes.dsi.unimi.it/~malchiod/SEI2/laboratorio/malformato.html");
			URL url = new URL("http://localhost/ifts/sei2/malformato.html");
			TagStatistics ts = new TagStatistics(url);
			ts.process();
			
			ts.printResults();
		} catch(MalformedURLException e) {
			e.printStackTrace();
		}
	}

}

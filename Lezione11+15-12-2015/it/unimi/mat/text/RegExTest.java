package it.unimi.mat.text;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegExTest {
	
	private static boolean printMatches(Matcher m) {
		boolean result = false;
		while (m.find()) {
			System.out.printf("I found the text \"%s\" starting at " +
		               "index %d and ending at index %d.%n",
		                m.group(), m.start(), m.end());
            result = true;
        }
		return result;
	}

    public static void main(String[] args){
    	
    	Pattern p = Pattern.compile("foo");
    	//Matcher m = p.matcher("foo");
    	//printMatches(m);
    	
    	//printMatches(p.matcher("foofoofoo"));
    	
//    	p = Pattern.compile("cat.");
//    	printMatches(p.matcher("cats"));
//    	printMatches(p.matcher("cate"));
//    	printMatches(p.matcher("category"));
    	
//    	p = Pattern.compile("[bcr]at");
//    	printMatches(p.matcher("cat, bat, rat, hat"));
    	
//    	p = Pattern.compile("[^bcr]at");
//    	printMatches(p.matcher("cat, bat, rat, hat"));
    	
//    	p = Pattern.compile("foo[1-5]");
//    	printMatches(p.matcher("foo1, foo2, foo5, foo6"));
    	
//    	p = Pattern.compile("[[0-4][6-8]]");
//    	//ma va bene anche "[0-4[6-8]]"
//    	printMatches(p.matcher("0123456789"));
    	
//    	p = Pattern.compile("[[0-9]&&[345]]");
//    	//ma va bene anche "[0-9&&[345]]"
//    	printMatches(p.matcher("0123456789"));
    	
//    	.	 Any character (may or may not match line terminators)
//    	\d	 A digit: [0-9]
//    	\D	 A non-digit: [^0-9]
//    	\s	 A whitespace character: [ \t\n\x0B\f\r]
//    	\S	 A non-whitespace character: [^\s]
//    	\w	 A word character: [a-zA-Z_0-9]
//    	\W	 A non-word character: [^\w]
    	
//    	p = Pattern.compile(".");
//    	printMatches(p.matcher("foo1\nfoo2"));
    	
//    	p = Pattern.compile("\\d");
//    	printMatches(p.matcher("foo1, foo2, foo5, foo6"));
    	
    	//p = Pattern.compile("c?");
    	//p = Pattern.compile("c+");
    	//p = Pattern.compile("c*");
    	//printMatches(p.matcher("c cciao ccciao"));
    	
//    	X?	  	X, once or not at all
//    	X*	 	X, zero or more times
//    	X+	  	X, one or more times
//    	X{n}	X, exactly n times
//    	X{n,}	X, at least n times
//    	X{n,m}	X, at least n but not more than m times
    	
//    	p = Pattern.compile(".*");
//    	
//    	p = Pattern.compile("(ciao)+");
//    	printMatches(p.matcher("oggi mi son detto ciao e poi ciaociao"));
//    	
//    	p = Pattern.compile("[abc]+");
//    	p = Pattern.compile("\\w+");
    	
//    	p = Pattern.compile(".*foo"); // greedy
//    	printMatches(p.matcher("xfooxxxxxxfoo"));
//    	
//    	p = Pattern.compile(".*?foo"); // reclutant
//    	printMatches(p.matcher("xfooxxxxxxfoo"));
//    	
//    	p = Pattern.compile(".*+foo"); // possessive
//    	printMatches(p.matcher("xfooxxxxxxfoo"));
    	
//    	p = Pattern.compile("(\\d\\d)\\1");
//    	printMatches(p.matcher("1212"));
//    	printMatches(p.matcher("1213"));
    	
//    	p = Pattern.compile("^dog$");
//    	printMatches(p.matcher("dog"));
//    	printMatches(p.matcher(" dog"));
    	
//    	p = Pattern.compile("^.*dog$");
//    	printMatches(p.matcher(" dog"));
//    	printMatches(p.matcher(" dog "));
    	
    	p = Pattern.compile("(\\w+).*");
    	System.out.println(p.matcher("uno due tre").replaceAll("$1"));	
    }
}

Questo repository viene reso disponibile per tutti gli studenti del corso di programmazione 3. In questo repository veranno caricate le slide del laboratorio e i codici per le esercitazioni. 


Come clonare il repository :

1) aprire il terminale e digitare: "git --version"
dovreste ottenere la versione installata di git.
qualora non fosse installato visitate (http://git-scm.com/)

2) posizionatevi nella cartella dove volete clonare il repository

3) clonate il repository digitando: 
"git clone https://ottobre23@bitbucket.org/ottobre23/lab.-programmazione-3.git"

 

Linee guida per l'interazione:

1)Siete pregati di creare il proprio account su bitbucket ed eseugire un fork del repository e da li effettuare le modifiche. (N.B. Senza la creazione è solo possibile clonare il repository ma non inviare in remoto le modifiche)

2)Sulla fork potete lavorare liberamente sul repository senza interefire con gli altri.

3)Prima di ogni lezione è importante controllare che non ci siano differenze tra il repository ufficiale e la fork, qualora ce ne fossero siete pregati di effettuare un merge.
 
se avete dei dubbi controllate le slide della lezione01


Owner & Admin: Marco Genuzio marco.genuzio@unimi.it
